package ru.m210projects.Launcher.desktop.newlauncher.stores;

import java.io.*;
import java.util.Objects;

public class WinReg {

	private static final String REGQUERY_UTIL = "reg query ";
	private static final String REGSTR_TOKEN = "REG_SZ";

	public static String getRegValue(String path, String key) {
		try {
			Process process;
			if (Objects.equals(key, "")) {
				process = Runtime.getRuntime().exec(REGQUERY_UTIL + "\"" + path + "\" /ve");
			} else {
				process = Runtime.getRuntime().exec(REGQUERY_UTIL + "\"" + path + "\" /v " + "\"" + key + "\"");
			}
			process.waitFor();

			StringBuilder buffer = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			while (reader.ready()) {
				buffer.append(reader.readLine());
			}
			String result = buffer.toString();
			int p = result.indexOf(REGSTR_TOKEN);
			if (p == -1) {
				return null;
			}

			return result.substring(p + REGSTR_TOKEN.length()).trim();
		} catch (Exception e) {
			return null;
		}
	}
}