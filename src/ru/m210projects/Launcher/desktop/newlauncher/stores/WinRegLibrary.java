package ru.m210projects.Launcher.desktop.newlauncher.stores;

import org.lwjgl.system.Platform;

import java.nio.file.Path;
import java.nio.file.Paths;

import static ru.m210projects.Build.filehandle.fs.FileEntry.DUMMY_PATH;

public class WinRegLibrary implements GameLibrary {

    private final String regKey;

    public WinRegLibrary(String regKey) {
        this.regKey = regKey;
    }

    @Override
    public Path resolvePath(String gamePath) {
        String pathToGame = (WinReg.getRegValue(gamePath, regKey));
        if (pathToGame != null) {
            return Paths.get(pathToGame);
        }

        return DUMMY_PATH;
    }

    @Override
    public boolean isPlatformSupported(Platform platform) {
        return platform == Platform.WINDOWS;
    }
}
