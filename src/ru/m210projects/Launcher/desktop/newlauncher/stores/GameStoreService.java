//This file is part of BuildGDX.
//Copyright (C) 2017-2021  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//BuildGDX is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//BuildGDX is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with BuildGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Launcher.desktop.newlauncher.stores;

import org.lwjgl.system.Platform;
import ru.m210projects.Launcher.desktop.newlauncher.entries.Game;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class GameStoreService {
    private final Map<Game, Set<GameLabel>> allGamePaths = new HashMap<>();
    private final Map<Store, List<GameLibrary>> storeLibraries = new HashMap<>();
    private final Map<Game, List<GameLabel>> checkedGamePaths = new HashMap<>();
    private final Platform platform;

    public GameStoreService(Platform platform) {
        this.platform = platform;

        storeLibraries.put(Store.STEAM, getSteamLibraries(platform));
        storeLibraries.put(Store.GOG, getGOGLibraries(platform));
        storeLibraries.put(Store.ZOOM, Collections.singletonList(new WinRegLibrary("InstallLocation")));
        storeLibraries.put(Store.ANTHOLOGY, Collections.singletonList(new WinRegLibrary("")));
    }

    public List<GameLabel> getGameLabels(Game game) {
        if (checkedGamePaths.containsKey(game)) {
            return checkedGamePaths.get(game);
        }

        List<GameLabel> result = new ArrayList<>();
        for (GameLabel label : allGamePaths.getOrDefault(game, new HashSet<>())) {
            List<GameLibrary> libraries = storeLibraries.getOrDefault(label.getStore(), new ArrayList<>());
            for (GameLibrary library : libraries) {
                if (!library.isPlatformSupported(platform)) {
                    continue;
                }

                Path path = library.resolvePath(label.getPath());
                if (Files.exists(path)) {
                    result.add(new GameLabel(label.getStore(), label.getLabel(), path.toString()));
                }
            }
        }

        checkedGamePaths.put(game, result);
        return result;
    }

    public void registerGame(Store store, Game entry, String... paths) {
        Set<GameLabel> list = allGamePaths.computeIfAbsent(entry, e -> new HashSet<>());
        for (String s : paths) {
            int index = s.indexOf(":\\");
            String label = s.substring(0, index);
            String path = s.substring(index + 2).trim();
            list.add(new GameLabel(store, label, path));
        }
    }

    private String getSteamFolder(Platform platform) {
        String steamInstall;
        switch (platform) {
            case LINUX:
                steamInstall = System.getProperty("user.home") + File.separator + ".steam" + File.separator + "steam";
                steamInstall = (steamInstall + File.separator + "steamapps" + File.separator);
                break;
            case WINDOWS:
                steamInstall = WinReg.getRegValue("HKCU\\Software\\Valve\\Steam", "SteamPath");
                if (steamInstall != null) {
                    steamInstall = (steamInstall + File.separator + "steamapps" + File.separator).replace("/",
                            File.separator);
                }
                break;
            case MACOSX:
                steamInstall = System.getProperty("user.home") + File.separator + "Library" + File.separator + "Application Support" + File.separator + "Steam";
                steamInstall = (steamInstall + File.separator + "steamapps" + File.separator);
                break;
            default:
                return null;
        }

        if (steamInstall != null) {
            File folder = new File(steamInstall);
            if (!folder.exists()) {
                return null;
            }
        }
        return steamInstall;
    }

    private List<GameLibrary> getSteamLibraries(Platform platform) {
        String steamInstall = getSteamFolder(platform);
        Set<GameLibrary> steamLibraries = new HashSet<>();
        if (steamInstall != null) {
            System.out.println("Found Steam folder");

            steamLibraries.add(new SteamLibrary(steamInstall + "common" + File.separator));

            String pathToVdf = steamInstall + "libraryfolders.vdf";
            Scanner input;
            try {
                input = new Scanner(new File(pathToVdf));
                String message = input.nextLine();
                if (message.equals("\"LibraryFolders\"")) {
                    while (input.hasNextLine()) {
                        message = input.nextLine();
                        if (message.startsWith("\t\"") && Character.isDigit(message.charAt(2))) {
                            String[] sentences = message.split("\t");
                            String s = sentences[sentences.length - 1];
                            s = s.replace("\"", "");
                            s = s.replace("\\\\", File.separator);
                            s = s + File.separator + "steamapps" + File.separator + "common" + File.separator;
                            steamLibraries.add(new SteamLibrary(s));
                        }
                    }
                } else if (message.equals("\"libraryfolders\"")) {
                    while (input.hasNextLine()) {
                        message = input.nextLine();
                        if (message.startsWith("\t\t\"path\"")) {
                            String[] sentences = message.split("\t");
                            String s = sentences[sentences.length - 1];
                            s = s.replace("\"", "");
                            s = s.replace("\\\\", File.separator);
                            s = s + File.separator + "steamapps" + File.separator + "common" + File.separator;
                            steamLibraries.add(new SteamLibrary(s));
                        }
                    }
                }
                input.close();
            } catch (Exception e) {
                e.printStackTrace();
                return new ArrayList<>(steamLibraries);
            }
        } else {
            System.out.println("Steam folder not found");
        }

        return new ArrayList<>(steamLibraries);
    }

    private List<GameLibrary> getGOGLibraries(Platform platform) {
        List<GameLibrary> gogLibraries = new ArrayList<>();
        switch (platform) {
            case LINUX:
                break;
            case WINDOWS:
                String[] gogpaths = {"HKLM\\Software\\WOW6432Node\\GOG.com", "HKLM\\Software\\GOG.com"};
                for (String path : gogpaths) {
                    if (WinReg.getRegValue(path, "") != null) {
                        gogLibraries.add(new GogLibrary(path));
                    }
                }
                break;
        }

        if (!gogLibraries.isEmpty()) {
            System.out.println("Found GOG folder");
        }

        return gogLibraries;
    }
}
