package ru.m210projects.Launcher.desktop.newlauncher.stores;

import org.lwjgl.system.Platform;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Objects;

import static ru.m210projects.Build.filehandle.fs.FileEntry.DUMMY_PATH;

public class GogLibrary implements GameLibrary {

    private final String storePath;

    public GogLibrary(String storePath) {
        this.storePath = storePath;
    }

    @Override
    public Path resolvePath(String gamePath) {
        String pathToGame = (WinReg.getRegValue(storePath + "\\" + gamePath, "path"));
        if (pathToGame != null) {
            return Paths.get(pathToGame);
        }

        return DUMMY_PATH;
    }

    @Override
    public boolean isPlatformSupported(Platform platform) {
        return platform == Platform.WINDOWS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GogLibrary that = (GogLibrary) o;
        return Objects.equals(storePath.toLowerCase(Locale.ROOT), that.storePath.toLowerCase(Locale.ROOT));
    }

    @Override
    public int hashCode() {
        return Objects.hash(storePath.toLowerCase(Locale.ROOT));
    }
}
