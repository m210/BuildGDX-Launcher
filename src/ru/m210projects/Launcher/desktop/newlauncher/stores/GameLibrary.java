package ru.m210projects.Launcher.desktop.newlauncher.stores;

import org.lwjgl.system.Platform;

import java.nio.file.Path;

public interface GameLibrary {

    Path resolvePath(String gamePath);

    boolean isPlatformSupported(Platform platform);
}
