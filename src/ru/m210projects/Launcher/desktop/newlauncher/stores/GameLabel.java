package ru.m210projects.Launcher.desktop.newlauncher.stores;

import java.util.Objects;

public class GameLabel {

    private final String label;
    private final String path;
    private final Store store;

    public GameLabel(Store store, String label, String path) {
        this.label = label;
        this.path = path;
        this.store = store;
    }

    public Store getStore() {
        return store;
    }

    public String getLabel() {
        return label;
    }

    public String getPath() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameLabel gameLabel = (GameLabel) o;
        return Objects.equals(label, gameLabel.label) && Objects.equals(path, gameLabel.path) && store == gameLabel.store;
    }

    @Override
    public int hashCode() {
        return Objects.hash(label, path, store);
    }
}
