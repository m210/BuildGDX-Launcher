package ru.m210projects.Launcher.desktop.newlauncher.stores;

import org.lwjgl.system.Platform;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Objects;

public class SteamLibrary implements GameLibrary {

    private final String steamLibraryPath;

    public SteamLibrary(String steamLibraryPath) {
        this.steamLibraryPath = steamLibraryPath;
        System.out.println(steamLibraryPath);

    }

    @Override
    public Path resolvePath(String gamePath) {
        return Paths.get(steamLibraryPath, gamePath);
    }

    @Override
    public boolean isPlatformSupported(Platform platform) {
        switch (platform) {
            case LINUX:
            case WINDOWS:
            case MACOSX:
                return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SteamLibrary that = (SteamLibrary) o;
        return Objects.equals(steamLibraryPath.toLowerCase(Locale.ROOT), that.steamLibraryPath.toLowerCase(Locale.ROOT));
    }

    @Override
    public int hashCode() {
        return Objects.hash(steamLibraryPath.toLowerCase(Locale.ROOT));
    }
}
