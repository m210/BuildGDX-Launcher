package ru.m210projects.Launcher.desktop.newlauncher;

import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.lwjgl3.audio.midi.device.SoundBankDevice;
import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.Architecture.common.audio.AudioDriver;
import ru.m210projects.Build.Architecture.common.audio.MidiDevice;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Launcher.desktop.Main;
import ru.m210projects.Launcher.desktop.newlauncher.entries.Game;
import ru.m210projects.Launcher.desktop.newlauncher.entries.GameEntry;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Stack;

public class Controller {

    private final LauncherModel model;
    private final Stack<Runnable> backStack = new Stack<>();
    private View view;
    private Runnable lastCallback;

    public Controller(LauncherModel model) {
        this.model = model;
    }

    public void setView(View view) {
        this.view = view;
        this.view.setController(this);
        view.setBackButtonEnable(false);
        this.onOpenAbout();
    }

    public void onStartGame() {
        if (model.getCurrentEntry().canStart()) {
            view.startGame(model::launchGame);
        } else {
            Main.showMessage("Error", String.format("Can't start because %s resources not found", model.getCurrentEntry().getGame().getName()), MessageType.Error);
        }
    }

    public void onChooseGame(Game game) {
        model.chooseGame(game);
        view.chooseGame(game);

        // to trig path checking and update start button or missing text panel
        onSetGamePath(model.getGamePath());

        if (lastCallback != null) {
            backStack.push(lastCallback);
            view.setBackButtonEnable(true);
        }

        lastCallback = () -> {
            model.chooseGame(game);
            view.chooseGame(game);
            onSetGamePath(model.getGamePath());
        };
    }

    public void onSetGamePath(Path path) {
        List<String> missingFiles = model.setGamePath(path);
        view.setGamePath(path);
        if (!missingFiles.isEmpty()) {
            view.showMissingFiles(missingFiles);
        }
    }

    public void onOpenSite() {
        view.openWebpage("https://m210.duke4.net/");
    }

    public void onOpenForum() {
        view.openWebpage("https://forums.duke4.net/forum/42-buildgdx/");
    }

    public void onOpenDiscord() {
        view.openWebpage("https://discord.gg/ebthbBB");
    }

    public void onChoosePath() {
        Path path = view.showDirectoryChooser(model.getGamePath(), model.getCurrentEntry());
        onSetGamePath(path);
    }

    public void onOpenGameFolder() {
        view.openFolder(model.getGamePath());
    }

    public void onOpenSettings() {
        final GameEntry currentEntry = model.getCurrentEntry();

        view.openSettings(currentEntry.getConfig());

        if (lastCallback != null) {
            backStack.push(lastCallback);
            view.setBackButtonEnable(true);
        }

        lastCallback = () -> {
            model.chooseGame(currentEntry.getGame());
            view.chooseGame(currentEntry.getGame());
            view.openSettings(currentEntry.getConfig());
        };
    }

    public void onOpenAbout() {
        view.openAbout();
        if (lastCallback != null) {
            backStack.push(lastCallback);
            view.setBackButtonEnable(true);
        }
        lastCallback = () -> view.openAbout();
    }

    public void onBack() {
        if (!backStack.isEmpty()) {
            lastCallback = backStack.pop();
            lastCallback.run();
        }
        view.setBackButtonEnable(!backStack.isEmpty());
    }

    public void resetBackStack() {
        backStack.removeAllElements();
        view.setBackButtonEnable(false);
    }

    public void onOpenConfigFolder() {
        Path configFolder = model.getCurrentEntry().getConfig().getCfgPath().getParent();
        if (!Files.exists(configFolder)) {
            try {
                Files.createDirectory(configFolder);
            } catch (IOException ignore) {
            }
        }
        view.openFolder(configFolder);
    }

    public void onChooseResolution(Graphics.DisplayMode displayMode) {
        GameConfig config = model.getCurrentEntry().getConfig();
        System.out.println("set display mode " + displayMode.width + " " + displayMode.height);
        config.setScreenMode(displayMode.width, displayMode.height, config.isFullscreen());
    }

    public void onChooseRender(Renderer.RenderType selectedRender) {
        model.getCurrentEntry().getConfig().setRenderType(selectedRender);
    }

    public void onChooseMidiDevice(MidiDevice device) {
        model.getCurrentEntry().getConfig().setMidiDevice(device);
        view.setMidiDevice(device);
    }

    public void onAddSoundBank(File file) {
        GameConfig config = model.getCurrentEntry().getConfig();
        try {
            MidiDevice.SoundBank device = new SoundBankDevice(file.toPath());
            config.addSoundBank(device);
            config.setMidiDevice(device);
            view.updateMidiDevices(config.getMidiDevices(), config.getMidiDevice().getName());
        } catch (Exception e) {
            Console.out.println(String.format("Can't add soundbank %s: %s", file.getName(), e));
        }
    }

    public void onDeleteSoundBank(MidiDevice device) {
        if (!(device instanceof SoundBankDevice)) {
            return;
        }

        GameConfig config = model.getCurrentEntry().getConfig();
        List<MidiDevice> list = config.getMidiDevices();
        int deviceIndex = list.indexOf(device);
        if (deviceIndex != -1) {
            list.remove(deviceIndex);
            device = list.get(Math.max(deviceIndex - 1, 0));
            config.setMidiDevice(device);
            view.updateMidiDevices(list, config.getMidiDevice().getName());
        }
    }

    public void onFullScreenChanged(boolean fullscreen) {
        GameConfig config = model.getCurrentEntry().getConfig(); // check
        config.setScreenMode(config.getScreenWidth(), config.getScreenHeight(), fullscreen);
    }

    public void onBorderlessChanged(boolean borderless) {
        model.getCurrentEntry().getConfig().setBorderless(borderless);
    }

    public void onEnableAutoloadChanged(boolean autoloadFolder) {
        model.getCurrentEntry().getConfig().setAutoloadFolder(autoloadFolder);
    }

    public void onChooseSound(AudioDriver audioDriver) {
        model.getCurrentEntry().getConfig().setAudioDriver(audioDriver);
    }
}
