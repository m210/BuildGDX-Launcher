package ru.m210projects.Launcher.desktop.newlauncher;

import com.badlogic.gdx.backends.LwjglLauncherUtil;
import com.badlogic.gdx.backends.lwjgl3.audio.OpenALAudio;
import com.badlogic.gdx.backends.lwjgl3.audio.midi.device.SoundBankDevice;
import org.jetbrains.annotations.NotNull;
import ru.m210projects.Build.Architecture.common.audio.AudioDriver;
import ru.m210projects.Build.Architecture.common.audio.BuildAudio;
import ru.m210projects.Build.Architecture.common.audio.MidiDevice;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Launcher.desktop.Main;
import ru.m210projects.Launcher.desktop.newlauncher.entries.Game;
import ru.m210projects.Launcher.desktop.newlauncher.entries.GameEntry;

import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static ru.m210projects.Build.filehandle.fs.FileEntry.DUMMY_PATH;
import static ru.m210projects.Launcher.desktop.newlauncher.entries.DummyEntry.DUMMY_ENTRY;

public class LauncherModel {

    private final List<MidiDevice> list = LwjglLauncherUtil.getMidiDevices();

    private final Map<Game, GameEntry> entries = new HashMap<>(32);
    private final Map<Game, Boolean> configLoaded = new HashMap<>(32);
    private final List<String> gameArgs;
    private GameEntry portableEntry;
    private GameEntry currentEntry = DUMMY_ENTRY;

    public LauncherModel(ArgumentsParser properties) throws AccessDeniedException {
        this.gameArgs = properties.getOtherArguments();

        Path homePath;
        if (!properties.showLauncher() && !properties.getPath().equals(DUMMY_PATH) && properties.getGame() != Game.NULL) {
            Game game = properties.getGame();
            homePath = properties.getPath();
            GameEntry entry = Utils.createGameEntry(game, homePath);
            if (!entry.equals(DUMMY_ENTRY)) {
                // put it to cache
                this.entries.put(game, entry);
                GameConfig config = entry.getConfig();
                if (isPortableGame(config) && entry.canStart()) {
                    // Don't start game at the time, we need to check can we write to this path
                    this.portableEntry = entry;
                }
            }
        } else {
            homePath = Utils.getHomePath();
        }

        if (!Files.isWritable(homePath)) {
            throw new AccessDeniedException("You don't have write permissions to path \"" + homePath + "\"");
        }

        // If the game selected by arguments passes permission check, we can launch it
        if (portableEntry != null) {
            this.currentEntry = portableEntry;
            return;
        }

        // Create all game entries
        Arrays.stream(Game.values()).forEach(game -> entries.computeIfAbsent(game, entry -> Utils.createGameEntry(entry, homePath)));

        // Check if the launcher was open in a game directory
        for (GameEntry entry : getEntries()) {
            if (!properties.showLauncher() && isPortableGame(entry.getConfig()) && entry.canStart()) {
                // Will start this game, don't have to init others
                this.portableEntry = entry;
                this.currentEntry = portableEntry;
                return;
            }
        }
    }

    /**
     * @param config game config file
     * @return true if a game folder contains the config file
     */
    private boolean isPortableGame(GameConfig config) {
        if (config == null || config.getGamePath().equals(DUMMY_PATH)) {
            return false;
        }

        Path relativePath = config.getGamePath().relativize(config.getCfgPath());
        return !relativePath.isAbsolute() && !relativePath.equals(config.getCfgPath());
    }

    public GameEntry getPortableEntry() {
        return portableEntry;
    }

    @NotNull
    public GameEntry getCurrentEntry() {
        return currentEntry;
    }

    public void chooseGame(Game game) {
        currentEntry = entries.get(game);
        loadConfig(currentEntry);
    }

    public Path getGamePath() {
        return currentEntry.getConfig().getGamePath();
    }

    public List<String> setGamePath(Path path) {
        return currentEntry.setGamePath(path);
    }

    public void launchGame() {
        loadConfig(currentEntry);

        // Don't launch the application from Swing thread to avoid blocking it by game loop
        new Thread(() -> currentEntry.runApplication(gameArgs, Main.appversion)).start();
    }

    public Set<GameEntry> getEntries() {
        return new HashSet<>(entries.values());
    }

    private void loadConfig(GameEntry entry) {
        configLoaded.computeIfAbsent(entry.getGame(), key -> {
            GameConfig config = entry.getConfig();
            config.registerAudioDriver(AudioDriver.DUMMY_AUDIO, new BuildAudio.DummyAudio(config));
            config.registerAudioDriver(AudioDriver.OPENAL_AUDIO, new OpenALAudio(config));
            config.addMidiDevices(list);
            config.setApplicationContext(new ConfigApplicationContext());
            config.load();

            for(Path path : config.getSoundBankPaths()) {
                if (Files.exists(path)) {
                    try {
                        config.addSoundBank(new SoundBankDevice(path));
                    } catch (Exception e) {
                        Console.out.println(String.format("Can't add soundbank %s: %s", path, e), OsdColor.RED);
                    }
                }
            }

            return true;
        });
    }
}
