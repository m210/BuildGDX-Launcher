package ru.m210projects.Launcher.desktop.newlauncher;

import com.badlogic.gdx.Graphics;
import ru.m210projects.Build.Architecture.common.ResolutionUtils;
import ru.m210projects.Build.Architecture.common.audio.AudioDriver;
import ru.m210projects.Build.Architecture.common.audio.MidiDevice;
import ru.m210projects.Build.settings.ApplicationContext;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static ru.m210projects.Build.Architecture.common.ResolutionUtils.getDisplayModeAsString;

/**
 * The context that doesn't open audio devices when chose by launcher
 */
public class ConfigApplicationContext extends ApplicationContext {

    @Override
    public void setAudioDriver(AudioDriver audioDriver) {
        this.audioDriver = audioDriver;
    }

    @Override
    public Map<String, List<Graphics.DisplayMode>> getResolutions() {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        java.awt.DisplayMode[] videoModes = gd.getDisplayModes();

        Set<Graphics.DisplayMode> modes = new LinkedHashSet<>();
        for (java.awt.DisplayMode videoMode : videoModes) {
            modes.add(new ResolutionUtils.UserDisplayMode(videoMode.getWidth(), videoMode.getHeight(), videoMode.getRefreshRate(), Math.max(1, videoMode.getBitDepth())));
        }

        return modes.stream().sorted(Comparator.comparingInt((Graphics.DisplayMode mode) -> mode.width)
                .thenComparingInt(mode -> mode.height)).collect(Collectors.groupingBy(e -> getDisplayModeAsString(e.width, e.height), LinkedHashMap::new, Collectors.toList()));
    }

    @Override
    public void setMidiDevice(MidiDevice midiDevice) {
        this.midiDevice = midiDevice.getName();
    }
}
