//This file is part of BuildGDX.
//Copyright (C) 2023  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//BuildGDX is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//BuildGDX is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with BuildGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Launcher.desktop.newlauncher.update;

import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Launcher.desktop.Main;
import ru.m210projects.Launcher.desktop.newlauncher.ui.DownloadDialog;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.CodeSource;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static ru.m210projects.Build.filehandle.fs.FileEntry.DUMMY_PATH;

public class VersionChecker implements Runnable {

    /**
     * Version of update application
     */
    public static final String UPDATER_VERSION = "1.0";
    public static final String VERSION_FILE = "buildgdx.ver";
    public static final String UPDATE_FILE = "update.zip";
    private final String serverUrl = "http://m210.ucoz.ru/Files/";
    private final ModelEventListener eventListener;
    private String serverVersion;

    public VersionChecker(ModelEventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override
    public void run() {
        CheckUpdateResponse response = checkVersion(Main.appversion);
        if (Objects.requireNonNull(response) == CheckUpdateResponse.UPDATE_AVAILABLE) {
            if (eventListener.onUpdateResult(CheckUpdateResponse.UPDATE_AVAILABLE, serverVersion)) {
                runUpdate();
            }
            return;
        }
        eventListener.onUpdateResult(CheckUpdateResponse.NOT_AVAILABLE, "");
    }

    public CheckUpdateResponse checkVersion(String version) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new DownloadInputStream(new URL(serverUrl + VERSION_FILE), 1000)))) {
            double serverVersion = Double.parseDouble(br.readLine().replaceAll("[^0-9.,]", ""));
            if (serverVersion == 117) {
                return CheckUpdateResponse.NOT_AVAILABLE;
            }

            double appVersion = Double.parseDouble(version.replaceAll("[^0-9.,]", ""));
            if (serverVersion <= appVersion) {
                return CheckUpdateResponse.NOT_AVAILABLE;
            }
            this.serverVersion = "v" + serverVersion;
        } catch (FileNotFoundException e) {
            return CheckUpdateResponse.VERSION_FILE_NOT_FOUND;
        } catch (Exception e) {
            return CheckUpdateResponse.VERSION_FILE_ERROR;
        }
        return CheckUpdateResponse.UPDATE_AVAILABLE;
    }

    public void runUpdate() {
        DownloadDialog dialog = new DownloadDialog();
        dialog.setVisible(true);

        AtomicBoolean isDismissed = new AtomicBoolean(false);
        dialog.setCancelListener(e -> {
            isDismissed.set(true);
            eventListener.onUpdateResult(CheckUpdateResponse.UPDATE_CANCELED, "Update canceled");
        });

        new Thread(() -> {
            Path updateFile;
            try (DownloadInputStream inputStream = new DownloadInputStream(new URL(serverUrl + UPDATE_FILE), 5000);
                 OutputStream outputStream = Files.newOutputStream(updateFile = Files.createTempFile(UPDATE_FILE, ""))) {
                updateFile.toFile().deleteOnExit();

                int len, size = 0;
                byte[] data = new byte[1024];
                int contentLength = inputStream.getContentLength();
                while ((len = inputStream.read(data)) != -1 && !isDismissed.get()) {
                    outputStream.write(data, 0, len);
                    size += len;
                    int percents = (int) ((size / (float) contentLength) * 100);
                    dialog.setProgress(percents);
                }

                if (isDismissed.get()) {
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
                eventListener.onUpdateResult(CheckUpdateResponse.UPDATE_ERROR, e.toString());
                return;
            } finally {
                dialog.dispose();
            }

            if (unpackUpdateFile(updateFile)) {
                try {
                    Path jarPath = getJarPath();
                    Path javaExecutablePath = getJavaExecutablePath();

                    Path updateApp = updateFile.getParent().resolve("appupdater.jar");
                    String command = String.format("%s -jar %s", javaExecutablePath, updateApp);

                    Process updaterProcess = Runtime.getRuntime().exec(command);
                    while (updaterProcess.isAlive()) {
                        if (updaterProcess.getInputStream().available() != 0) {
                            BufferedReader reader = new BufferedReader(new InputStreamReader(updaterProcess.getInputStream()));
                            while (reader.ready()) {
                                String message = reader.readLine();
                                if (message.equals("Updater started")) {
                                    UpdateParameters parameters = new UpdateParameters(UPDATER_VERSION, javaExecutablePath, jarPath, updateApp.getParent(), Main.appversion);
                                    parameters.sendData(updaterProcess.getOutputStream());
                                } else if (message.equals("Ready to replace")) {
                                    // exit the current application to replace jar-file
                                    System.exit(0);
                                }
                            }
                        } else {
                            if (updaterProcess.getErrorStream().available() != 0) {
                                BufferedReader reader = new BufferedReader(new InputStreamReader(updaterProcess.getErrorStream()));
                                StringBuilder stringBuffer = new StringBuilder();
                                while (reader.ready()) {
                                    stringBuffer.append(reader.readLine());
                                }
                                Console.out.println(stringBuffer.toString(), OsdColor.RED);
                                throw new IOException(stringBuffer.toString());
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    eventListener.onUpdateResult(CheckUpdateResponse.UPDATE_ERROR, e.toString());
                }
            } else {
                eventListener.onUpdateResult(CheckUpdateResponse.UPDATE_ERROR, "Can't unpack an update file.");
            }
        }).start();
    }

    private Path getJarPath() {
        CodeSource codeSource = getClass().getProtectionDomain().getCodeSource();
        if (codeSource != null) {
            try {
                Path path = Paths.get(codeSource.getLocation().toURI());
                if (path.getFileName() != null) {
                    return path;
                }
            } catch (URISyntaxException ignore) {
            }
        }
        return DUMMY_PATH;
    }

    private boolean unpackUpdateFile(Path file) {
        byte[] buffer = new byte[1024];
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(file.toFile()))) {
            ZipEntry zipEntry = zis.getNextEntry();
            if (zipEntry != null) {
                while (zipEntry != null) {
                    try (FileOutputStream fos = new FileOutputStream(file.getParent().resolve(zipEntry.getName()).toFile())) {
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                        }
                    }
                    zis.closeEntry();
                    zipEntry = zis.getNextEntry();
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Path getJavaExecutablePath() {
        Path javaHome = FileUtils.getPath(System.getProperty("java.home"));
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            return javaHome.resolve("bin").resolve("java.exe");
        }
        return javaHome.resolve("bin").resolve("java");
    }
}
