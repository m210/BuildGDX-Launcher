//This file is part of BuildGDX.
//Copyright (C) 2024  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//BuildGDX is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//BuildGDX is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with BuildGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Launcher.desktop.newlauncher.update;

import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AppUpdater {
    public static final String VERSION = "1.0";

    public static void main(String[] args) throws Exception {
        System.out.println("Updater started");
        final int timeout = 5000;
        long startWait = System.currentTimeMillis();
        while (System.in.available() == 0) {
            if (System.currentTimeMillis() - startWait >= timeout) {
                showError("Parameters input timeout");
                throw new RuntimeException("Parameters input timeout");
            }
        }

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String updaterVersion = reader.readLine();
            if (!VERSION.equals(updaterVersion)) {
                showError("Updater version is wrong!");
                throw new RuntimeException("Updater version is wrong!");
            }

            String applicationName = "update.jar";
            Path javaExecutablePath = Paths.get(reader.readLine());
            Path applicationPath = Paths.get(reader.readLine());
            String currentVersion = reader.readLine();
            Path tempDirectory = Paths.get(reader.readLine());

            try (Stream<Path> stream = Files.list(tempDirectory)) {
                List<Path> pathList = stream.map(Path::normalize).filter(Files::isRegularFile).collect(Collectors.toList());
                Path src = pathList.stream().filter(Files::isRegularFile).filter(p -> p.getFileName().toString().equalsIgnoreCase(applicationName)).findFirst().orElse(Paths.get(applicationName));
                if (!Files.exists(src)) {
                    showError("Can't update the application, because the file \"" + src.getFileName() + "\" is not found");
                    throw new FileNotFoundException(src.getFileName().toString());
                }

                System.out.println("Ready to replace");
                if (renameOldApplication(applicationPath, currentVersion)) {
                    Files.move(src, applicationPath, StandardCopyOption.REPLACE_EXISTING);
                    String command = String.format("%s -jar %s -updated", javaExecutablePath, applicationPath);
                    Runtime.getRuntime().exec(command);
                } else {
                    showError("Can't update the application, while file \"" + applicationPath + "\" is locked");
                    throw new RuntimeException("Update error, because file " + applicationPath + " is locked");
                }
            }
        }
    }

    public static boolean renameOldApplication(Path path, String version) throws InterruptedException {
        if (!Files.exists(path)) {
            return true;
        }
        String filename = path.toFile().getName();
        if (filename.contains(".")) {
            filename = filename.substring(0, filename.lastIndexOf('.'));
        }
        filename += "_" + version + ".jar";
        int repeats = 0;
        String errorMessage = "";
        while (repeats != 50) {
            try {
                Files.move(path, path.resolveSibling(filename), StandardCopyOption.REPLACE_EXISTING);
                return true;
            } catch (Exception e) {
                errorMessage = e.toString();
            }
            Thread.sleep(300);
            repeats++;
        }
        showError(errorMessage);
        return false;
    }

    private static void showError(String message) {
        JOptionPane panel = new JOptionPane();
        panel.setMessageType(JOptionPane.ERROR_MESSAGE);
        panel.setMessage(message);
        panel.setOptionType(JOptionPane.DEFAULT_OPTION);
        JDialog dialog = panel.createDialog("Update error");
        dialog.setAlwaysOnTop(true);
        dialog.pack();
        dialog.setVisible(true);
        // We should call dispose to release AWT thread
        dialog.dispose();

        writeLog(message);
    }

    private static void writeLog(String message) {
        try {
            FileWriter writer = new FileWriter("appupdater.log", true);
            LocalDateTime date = LocalDateTime.now();
            writer.write(String.format("[%s] Update error: %s\n", date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")), message));
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
