//This file is part of BuildGDX.
//Copyright (C) 2024  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//BuildGDX is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//BuildGDX is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with BuildGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Launcher.desktop.newlauncher.update;

import java.io.*;
import java.nio.file.Path;

import static ru.m210projects.Launcher.desktop.newlauncher.update.VersionChecker.UPDATER_VERSION;

public class UpdateParameters implements Serializable {
    /**
     * Version of updater app
     */
    private final String updaterVersion;

    /**
     * Path to java.exe (in case to use portable version of java? should be possible to launch it after update)
     */
    private final Path javaExecutablePath;

    /**
     * Path to app jar file for ability to replace old app version to new one by this path
     */
    private final Path applicationPath;

    /**
     * Information about old app version to keep and rename the jar file like buildgdx_v0.99.jar
     */
    private final String currentVersion;

    /**
     * Path where is located the new app version
     */
    private final Path tempDirectory; // path where is located appupdater and new app version.

    public UpdateParameters(String updaterVersion, Path javaExecutablePath, Path applicationPath, Path tempDirectory, String currentVersion) {
        this.updaterVersion = updaterVersion;
        this.javaExecutablePath = javaExecutablePath;
        this.applicationPath = applicationPath;
        this.currentVersion = currentVersion;
        this.tempDirectory = tempDirectory;
    }

    /**
     * Sends parameters to the updater when it is ready to apply them
     * @param outputStream AppUpdater stream
     */
    public void sendData(OutputStream outputStream) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
        writer.write(updaterVersion);
        writer.newLine();
        writer.write(javaExecutablePath.toString());
        writer.newLine();
        writer.write(applicationPath.toString());
        writer.newLine();
        writer.write(currentVersion);
        writer.newLine();
        writer.write(tempDirectory.toString());
        writer.newLine();
        writer.flush();
    }
}
