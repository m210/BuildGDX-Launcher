package ru.m210projects.Launcher.desktop.newlauncher.update;

public enum CheckUpdateResponse {
    NOT_AVAILABLE,
    VERSION_FILE_ERROR,
    VERSION_FILE_NOT_FOUND,
    UPDATE_CANCELED,
    UPDATE_ERROR,
    UPDATE_AVAILABLE;
}