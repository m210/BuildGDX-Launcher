package ru.m210projects.Launcher.desktop.newlauncher.update;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadInputStream extends InputStream {

    private final HttpURLConnection connection;
    private final InputStream inputStream;
    private final int contentLength;

    public DownloadInputStream(URL url, int timeout) throws IOException {
        connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(timeout);
        inputStream = new BufferedInputStream(connection.getInputStream());
        contentLength = connection.getContentLength();
    }

    public int getContentLength() {
        return contentLength;
    }

    @Override
    public int read(byte[] b) throws IOException {
        return inputStream.read(b);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return inputStream.read(b, off, len);
    }

    @Override
    public long skip(long n) throws IOException {
        return inputStream.skip(n);
    }

    @Override
    public int available() throws IOException {
        return inputStream.available();
    }

    @Override
    public int read() throws IOException {
        return inputStream.read();
    }

    @Override
    public void close() throws IOException {
        if (connection != null) {
            connection.disconnect();
        }
        super.close();
    }
}