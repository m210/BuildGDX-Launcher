package ru.m210projects.Launcher.desktop.newlauncher.update;

public interface ModelEventListener {

    boolean onUpdateResult(CheckUpdateResponse response, String resultText);

}
