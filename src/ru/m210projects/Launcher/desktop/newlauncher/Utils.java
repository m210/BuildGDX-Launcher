package ru.m210projects.Launcher.desktop.newlauncher;

import org.jetbrains.annotations.NotNull;
import org.lwjgl.system.Platform;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Launcher.desktop.newlauncher.entries.*;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static ru.m210projects.Launcher.desktop.newlauncher.entries.DummyEntry.DUMMY_ENTRY;

public class Utils {

    public static Path getHomePath() {
        Path homePath = Paths.get(System.getProperty("user.home"), Platform.get() == Platform.LINUX ? ".m210projects" : "M210Projects");
        File f = homePath.toFile();
        if (!f.exists() && !f.mkdirs() && !f.isDirectory()) {
            Console.out.println("Can't create path \"" + homePath + '"', OsdColor.RED);
        }

        return homePath;
    }

    public static Path getDirPath() {
        Platform platform = Platform.get();
        if (platform == Platform.LINUX) {
            String parent = System.getProperty("java.class.path");
            if (parent != null) {
                return Paths.get(parent).toAbsolutePath();
            }
        }
        return Paths.get(System.getProperty("user.dir"));
    }

    @NotNull
    public static GameEntry createGameEntry(Game game, Path path) {
        switch (game) {
            case BLOOD:
                return new BloodEntry(path);
            case DUKE_NUKEM_3D:
                return new Duke3DEntry(path);
            case NAM:
                return new NamEntry(path);
            case SHADOW_WARRIOR:
                return new SWEntry(path);
            case REDNECK_RAMPAGE:
                return new RREntry(path);
            case RR_RIDES_AGAIN:
                return new RRRAEntry(path);
            case POWERSLAVE:
                return new PSlaveEntry(path);
            case TEKWAR:
                return new TekwarEntry(path);
            case WITCHAVEN:
                return new WHEntry(path);
            case WITCHAVEN_2:
                return new WH2Entry(path);
            case LEGEND_OF_THE_SEVEN_PALADINS:
                return new LSPEntry(path);
        }
        return DUMMY_ENTRY;
    }
}
