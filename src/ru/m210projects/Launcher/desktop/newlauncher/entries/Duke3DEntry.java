package ru.m210projects.Launcher.desktop.newlauncher.entries;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Duke3D.Config;
import ru.m210projects.Duke3D.Main;
import ru.m210projects.Launcher.desktop.newlauncher.stores.GameStoreService;
import ru.m210projects.Launcher.desktop.newlauncher.stores.Store;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static ru.m210projects.Launcher.desktop.Main.headerPath;
import static ru.m210projects.Launcher.desktop.Main.iconPath;

public class Duke3DEntry implements GameEntry {

    private final String appName = "DukeGDX";
    private final GameConfig entryConfig;
    private List<String> missingFilesCache;

    public Duke3DEntry(Path homePath) {
        missingFilesCache = checkAndGetMissingFiles(homePath);
        boolean portable = missingFilesCache.isEmpty();
        String configName = (appName + ".ini").toLowerCase(Locale.ROOT);
        if (portable) {
            entryConfig = new Config(homePath.resolve(Paths.get(configName)));
            entryConfig.setGamePath(homePath);
        } else {
            entryConfig = new Config(homePath.resolve(Paths.get(appName, configName)));
        }
    }

    @Override
    public URL getLogo() {
        return getClass().getResource(headerPath + "/headerduke.png");
    }

    @Override
    public Game getGame() {
        return Game.DUKE_NUKEM_3D;
    }

    @Override
    public String[] getFiles() {
        return new String[]{"duke3d.grp"};
    }

    @Override
    public String[] getIcons() {
        return new String[]{iconPath + "/Duke3D/duke16.png",
                iconPath + "/Duke3D/duke32.png",
                iconPath + "/Duke3D/duke128.png"};
    }

    @Override
    public BuildGame getApplication(List<String> args, String version) throws IOException {
        return new Main(args, entryConfig, appName, version, 0);
    }

    @Override
    public GameConfig getConfig() {
        return entryConfig;
    }

    @Override
    public List<String> setGamePath(Path path) {
        entryConfig.setGamePath(path);
        missingFilesCache = checkAndGetMissingFiles(path);
        return missingFilesCache;
    }

    @Override
    public boolean canStart() {
        return missingFilesCache.isEmpty();
    }

    @Override
    public void registerStorePath(GameStoreService gameStoreService) {
        gameStoreService.registerGame(Store.GOG, getGame(), "[GOG]:\\" + "GOGDUKE3D");

        gameStoreService.registerGame(Store.STEAM, getGame(),
                "[WT Steam]:\\" + "Duke Nukem 3D Twentieth Anniversary World Tour",
                "[Megaton Steam]:\\" + "Duke Nukem 3D" + File.separator + "gameroot",
                "[3DRA Steam]:\\" + "Duke Nukem 3D" + File.separator + "Duke Nukem 3D");

        gameStoreService.registerGame(Store.ZOOM, getGame(),
                "[Zoom]:\\" + "HKLM\\Software\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{701DE32E-4F6E-4524-9966-837165B91EB9}_is1",
                "[Zoom]:\\" + "HKLM\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{701DE32E-4F6E-4524-9966-837165B91EB9}_is1");

        gameStoreService.registerGame(Store.ANTHOLOGY, getGame(),
                "[Anthology]:\\" + "HKLM\\SOFTWARE\\WOW6432Node\\3DRealms\\Anthology",
                "[Anthology]:\\" + "HKLM\\SOFTWARE\\3DRealms\\Anthology",
                "[Anthology]:\\" + "HKLM\\SOFTWARE\\WOW6432Node\\3DRealms\\Duke Nukem 3D",
                "[Anthology]:\\" + "HKLM\\SOFTWARE\\3DRealms\\Duke Nukem 3D");
    }
}
