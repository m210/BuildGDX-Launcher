package ru.m210projects.Launcher.desktop.newlauncher.entries;

import ru.m210projects.Blood.Config;
import ru.m210projects.Blood.Main;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Launcher.desktop.newlauncher.stores.GameStoreService;
import ru.m210projects.Launcher.desktop.newlauncher.stores.Store;

import java.io.IOException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static ru.m210projects.Launcher.desktop.Main.headerPath;
import static ru.m210projects.Launcher.desktop.Main.iconPath;

public class BloodEntry implements GameEntry {

    private final String appName = "BloodGDX";
    private final GameConfig entryConfig;
    private List<String> missingFilesCache;
    private boolean isDemo = false;

    public BloodEntry(Path homePath) {
        this.missingFilesCache = checkAndGetMissingFiles(homePath);
        boolean portable = missingFilesCache.isEmpty();
        String configName = (appName + ".ini").toLowerCase(Locale.ROOT);
        if (portable) {
            entryConfig = new Config(homePath.resolve(Paths.get(configName)));
            entryConfig.setGamePath(homePath);
        } else {
            entryConfig = new Config(homePath.resolve(Paths.get(appName, configName)));
        }
    }

    @Override
    public URL getLogo() {
        // ClassLoader.getSystemResourceAsStream(Paths.get(headerPath, "headerblood.png").toString());
        return getClass().getResource(headerPath + "/headerblood.png");
    }

    @Override
    public Game getGame() {
        return Game.BLOOD;
    }

    @Override
    public String[] getFiles() {
        return new String[]{"blood.ini", "blood.rff", "sounds.rff", "tiles000.art", "tables.dat", "surface.dat"};
    }

    @Override
    public String[] getIcons() {
        return new String[]{iconPath + "/Blood/blood16.png", iconPath + "/Blood/blood32.png", iconPath + "/Blood/blood128.png"};
    }

    @Override
    public List<String> checkAndGetMissingFiles(Path directoryPath) {
        isDemo = false;
        List<String> missingFiles = GameEntry.super.checkAndGetMissingFiles(directoryPath);
        if (!missingFiles.isEmpty()) {
            System.out.println(appName + ": trying to find demo version");
            List<String> missingDemoFiles = checkAndGetMissingDemoFiles(directoryPath);
            if (missingDemoFiles.isEmpty()) {
                isDemo = true;
                return missingDemoFiles;
            }
            missingFiles.addAll(missingDemoFiles);
        }
        return missingFiles;
    }

    @Override
    public BuildGame getApplication(List<String> args, String version) throws IOException {
        return new Main(args, entryConfig, appName, version, isDemo);
    }

    @Override
    public GameConfig getConfig() {
        return entryConfig;
    }

    @Override
    public List<String> setGamePath(Path path) {
        entryConfig.setGamePath(path);
        missingFilesCache = checkAndGetMissingFiles(path);
        return missingFilesCache;
    }

    @Override
    public boolean canStart() {
        return missingFilesCache.isEmpty();
    }

    @Override
    public void registerStorePath(GameStoreService gameStoreService) {
        gameStoreService.registerGame(Store.GOG, getGame(), //
                "[OUWB GOG]:\\" + "GOGONEUNITONEBLOOD", //
                "[FS GOG]:\\" + "Games\\1374469660"); //

        gameStoreService.registerGame(Store.STEAM, getGame(),
                "[OUWB Steam]:\\" + "One Unit Whole Blood", //
                "[FS Steam]:\\" + "Blood"); //
    }

    /**
     * Tries to find demo game in the directory path
     */
    private List<String> checkAndGetMissingDemoFiles(Path directoryPath) {
        Set<String> missingFiles = new LinkedHashSet<>(Arrays.asList("blood.ini", "blood.rff", "sounds.rff", "share000.art", "tables.dat", "surface.dat"));
        if (Files.exists(directoryPath)) {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(directoryPath)) {
                stream.forEach(path -> missingFiles.remove(path.getFileName().toString().toLowerCase(Locale.ROOT)));
            } catch (IOException e) {
                Console.out.println(String.format("Failed to check path %s: %s", directoryPath, e), OsdColor.RED);
            }
        }
        return new ArrayList<>(missingFiles);
    }
}
