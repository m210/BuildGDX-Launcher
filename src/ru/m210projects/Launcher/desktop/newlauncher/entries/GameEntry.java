package ru.m210projects.Launcher.desktop.newlauncher.entries;

import com.badlogic.gdx.backends.LwjglLauncherUtil;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Launcher.desktop.Main;
import ru.m210projects.Launcher.desktop.newlauncher.stores.GameStoreService;

import java.io.IOException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public interface GameEntry {

    URL getLogo();

    Game getGame();

    String[] getFiles();

    String[] getIcons();

    BuildGame getApplication(List<String> args, String version) throws IOException;

    GameConfig getConfig();

    List<String> setGamePath(Path path);

    boolean canStart();

    default void registerStorePath(GameStoreService gameStoreService) {
    }

    default void runApplication(List<String> args, String version) {
        if (!canStart()) {
            return;
        }

        try {
            LwjglLauncherUtil.launch(getApplication(args, version), getIcons());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    default List<String> checkAndGetMissingFiles(Path directoryPath) {
        Set<String> missingFiles = new LinkedHashSet<>(Arrays.asList(getFiles()));
        if (Files.exists(directoryPath)) {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(directoryPath)) {
                stream.forEach(path -> missingFiles.remove(path.getFileName().toString().toLowerCase(Locale.ROOT)));
            } catch (IOException e) {
                Console.out.println(String.format("Failed to check path %s: %s", directoryPath, e), OsdColor.RED);
            }
        }
        return new ArrayList<>(missingFiles);
    }
}
