package ru.m210projects.Launcher.desktop.newlauncher.entries;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.settings.GameConfig;

import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class DummyEntry implements GameEntry {

    public static final DummyEntry DUMMY_ENTRY = new DummyEntry();

    @Override
    public URL getLogo() {
        return null;
    }

    @Override
    public Game getGame() {
        return Game.NULL;
    }

    @Override
    public String[] getFiles() {
        return new String[0];
    }

    @Override
    public String[] getIcons() {
        return new String[0];
    }

    @Override
    public BuildGame getApplication(List<String> args, String version) {
        return null;
    }

    @Override
    public GameConfig getConfig() {
        return null;
    }

    @Override
    public List<String> setGamePath(Path path) {
        return new ArrayList<>();
    }

    @Override
    public boolean canStart() {
        return false;
    }

}
