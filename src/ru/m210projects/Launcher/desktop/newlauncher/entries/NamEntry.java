package ru.m210projects.Launcher.desktop.newlauncher.entries;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Duke3D.Config;
import ru.m210projects.Duke3D.Main;
import ru.m210projects.Launcher.desktop.newlauncher.stores.GameStoreService;
import ru.m210projects.Launcher.desktop.newlauncher.stores.Store;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static ru.m210projects.Launcher.desktop.Main.headerPath;
import static ru.m210projects.Launcher.desktop.Main.iconPath;

public class NamEntry implements GameEntry {

    private final String appName = "NamGDX";
    private final GameConfig entryConfig;
    private List<String> missingFilesCache;

    public NamEntry(Path homePath) {
        missingFilesCache = checkAndGetMissingFiles(homePath);
        boolean portable = missingFilesCache.isEmpty();
        String configName = (appName + ".ini").toLowerCase(Locale.ROOT);
        if (portable) {
            entryConfig = new Config(homePath.resolve(Paths.get(configName)));
            entryConfig.setGamePath(homePath);
        } else {
            entryConfig = new Config(homePath.resolve(Paths.get(appName, configName)));
        }
    }

    @Override
    public URL getLogo() {
        return getClass().getResource(headerPath + "/headernam.png");
    }

    @Override
    public Game getGame() {
        return Game.NAM;
    }

    @Override
    public String[] getFiles() {
        return new String[]{"nam.grp", "game.con"};
    }

    @Override
    public String[] getIcons() {
        return new String[]{
                iconPath + "/Nam/nam16.png",
                iconPath + "/Nam/nam32.png",
                iconPath + "/Nam/nam128.png"
        };
    }

    @Override
    public List<String> checkAndGetMissingFiles(Path directoryPath) {
        List<String> missingFiles = GameEntry.super.checkAndGetMissingFiles(directoryPath);
        if (!missingFiles.isEmpty()) {
            System.out.println(appName + ": trying to find another version");
            List<String> missingNapalmFiles = checkAndGetMissingNapalmFiles(directoryPath);
            if (missingNapalmFiles.isEmpty()) {
                return missingNapalmFiles;
            }
            missingFiles.addAll(missingNapalmFiles);
        }
        return missingFiles;
    }

    @Override
    public BuildGame getApplication(List<String> args, String version) throws IOException {
        return new Main(args, entryConfig, appName, version, 1, true);
    }

    @Override
    public GameConfig getConfig() {
        return entryConfig;
    }

    @Override
    public List<String> setGamePath(Path path) {
        entryConfig.setGamePath(path);
        missingFilesCache = checkAndGetMissingFiles(path);
        return missingFilesCache;
    }

    @Override
    public boolean canStart() {
        return missingFilesCache.isEmpty();
    }

    @Override
    public void registerStorePath(GameStoreService gameStoreService) {
        gameStoreService.registerGame(Store.GOG, getGame(),
                "[GOG]:\\" + "Games\\1575726518");

        gameStoreService.registerGame(Store.STEAM, getGame(),
                "[Steam]:\\" + "Nam" + File.separator + "NAM");
    }

    /**
     * Tries to find demo game in the directory path
     */
    private List<String> checkAndGetMissingNapalmFiles(Path directoryPath) {
        Set<String> missingFiles = new LinkedHashSet<>(Arrays.asList("napalm.grp", "game.con"));
        if (Files.exists(directoryPath)) {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(directoryPath)) {
                stream.forEach(path -> missingFiles.remove(path.getFileName().toString().toLowerCase(Locale.ROOT)));
            } catch (IOException e) {
                Console.out.println(String.format("Failed to check path %s: %s", directoryPath, e), OsdColor.RED);
            }
        }
        return new ArrayList<>(missingFiles);
    }
}
