package ru.m210projects.Launcher.desktop.newlauncher.entries;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.LSP.Config;
import ru.m210projects.LSP.Main;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static ru.m210projects.Launcher.desktop.Main.headerPath;
import static ru.m210projects.Launcher.desktop.Main.iconPath;

public class LSPEntry implements GameEntry {

    private final String appName = "LSPGDX";
    private final GameConfig entryConfig;
    private List<String> missingFilesCache;

    public LSPEntry(Path homePath) {
        missingFilesCache = checkAndGetMissingFiles(homePath);
        boolean portable = missingFilesCache.isEmpty();
        String configName = (appName + ".ini").toLowerCase(Locale.ROOT);
        if (portable) {
            entryConfig = new Config(homePath.resolve(Paths.get(configName)));
            entryConfig.setGamePath(homePath);
        } else {
            entryConfig = new Config(homePath.resolve(Paths.get(appName, configName)));
        }
    }

    @Override
    public URL getLogo() {
        return getClass().getResource(headerPath + "/headerlot7p.png");
    }

    @Override
    public Game getGame() {
        return Game.LEGEND_OF_THE_SEVEN_PALADINS;
    }

    @Override
    public String[] getFiles() {
        return new String[]{"l0art000.dat", "l1art000.dat", "lmart000.dat", "palette.dat", "tables.dat"};
    }

    @Override
    public String[] getIcons() {
        return new String[]{iconPath + "/Lot7P/lot7p16.png", iconPath + "/Lot7P/lot7p32.png", iconPath + "/Lot7P/lot7p128.png"};
    }

    @Override
    public BuildGame getApplication(List<String> args, String version) throws IOException {
        return new Main(args, entryConfig, appName, version, true);
    }

    @Override
    public GameConfig getConfig() {
        return entryConfig;
    }

    @Override
    public List<String> setGamePath(Path path) {
        entryConfig.setGamePath(path);
        missingFilesCache = checkAndGetMissingFiles(path);
        return missingFilesCache;
    }

    @Override
    public boolean canStart() {
        return missingFilesCache.isEmpty();
    }

}
