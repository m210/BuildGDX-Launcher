package ru.m210projects.Launcher.desktop.newlauncher.entries;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Launcher.desktop.newlauncher.stores.GameStoreService;
import ru.m210projects.Launcher.desktop.newlauncher.stores.Store;
import ru.m210projects.Powerslave.Config;
import ru.m210projects.Powerslave.Main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static ru.m210projects.Launcher.desktop.Main.headerPath;
import static ru.m210projects.Launcher.desktop.Main.iconPath;

public class PSlaveEntry implements GameEntry {

    private final String appName = "PowerslaveGDX";
    private final GameConfig entryConfig;
    private List<String> missingFilesCache;

    public PSlaveEntry(Path homePath) {
        missingFilesCache = checkAndGetMissingFiles(homePath);
        boolean portable = missingFilesCache.isEmpty();
        String configName = (appName + ".ini").toLowerCase(Locale.ROOT);
        if (portable) {
            entryConfig = new Config(homePath.resolve(Paths.get(configName)));
            entryConfig.setGamePath(homePath);
        } else {
            entryConfig = new Config(homePath.resolve(Paths.get(appName, configName)));
        }
    }

    @Override
    public URL getLogo() {
        return getClass().getResource(headerPath + "/headerps.png");
    }

    @Override
    public Game getGame() {
        return Game.POWERSLAVE;
    }

    @Override
    public String[] getFiles() {
        return new String[]{"stuff.dat"};
    }

    @Override
    public String[] getIcons() {
        return new String[]{iconPath + "/PS/ps16.png", iconPath + "/PS/ps32.png", iconPath + "/PS/ps128.png"};
    }

    @Override
    public BuildGame getApplication(List<String> args, String version) throws IOException {
        return new Main(args, entryConfig, appName, version, true);
    }

    @Override
    public GameConfig getConfig() {
        return entryConfig;
    }

    @Override
    public List<String> setGamePath(Path path) {
        entryConfig.setGamePath(path);
        missingFilesCache = checkAndGetMissingFiles(path);
        return missingFilesCache;
    }

    @Override
    public boolean canStart() {
        return missingFilesCache.isEmpty();
    }

    @Override
    public void registerStorePath(GameStoreService gameStoreService) {
        gameStoreService.registerGame(Store.GOG, getGame(), "[GOG]:\\" + "Games\\2132611980");

        gameStoreService.registerGame(Store.STEAM, getGame(), "[Steam]:\\" + "PowerslaveCE" + File.separator + "PWRSLAVE");
    }
}
