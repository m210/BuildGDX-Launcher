package ru.m210projects.Launcher.desktop.newlauncher.entries;

public enum Game {
    NULL(""),
    BLOOD("Blood"),
    DUKE_NUKEM_3D("Duke Nukem 3D"),
    LEGEND_OF_THE_SEVEN_PALADINS("Seven Paladins", "Lot7P"),
    NAM("NAM"),
    POWERSLAVE("Powerslave/Exhumed", "Powerslave"),
    REDNECK_RAMPAGE("Redneck Rampage"),
    RR_RIDES_AGAIN("Redneck Rampage: Rides Again", "RR Rides Again"),
    SHADOW_WARRIOR("Shadow Warrior"),
    TEKWAR("TekWar"),
    WITCHAVEN("Witchaven"),
    WITCHAVEN_2("Witchaven II");

    private final String name;
    private final String shortName;

    Game(String name, String shortName) {
        this.name = name;
        this.shortName = shortName;
    }

    Game(String name) {
        this.name = name;
        this.shortName = name;
    }

    public static Game parseGame(String name) {
        for(Game game : Game.values()) {
            if (game.name().equalsIgnoreCase((name)) || game.shortName.equalsIgnoreCase(name) || game.name.equalsIgnoreCase(name)) {
                return game;
            }
        }
        return Game.NULL;
    }
    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }
}
