package ru.m210projects.Launcher.desktop.newlauncher.entries;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Launcher.desktop.newlauncher.stores.GameStoreService;
import ru.m210projects.Launcher.desktop.newlauncher.stores.Store;
import ru.m210projects.Redneck.Config;
import ru.m210projects.Redneck.Main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static ru.m210projects.Launcher.desktop.Main.headerPath;
import static ru.m210projects.Launcher.desktop.Main.iconPath;

public class RRRAEntry implements GameEntry {

    private final String appName = "RedneckAgainGDX";
    private final GameConfig entryConfig;
    private List<String> missingFilesCache;

    public RRRAEntry(Path homePath) {
        missingFilesCache = checkAndGetMissingFiles(homePath);
        boolean portable = missingFilesCache.isEmpty();
        String configName = (appName + ".ini").toLowerCase(Locale.ROOT);
        if (portable) {
            entryConfig = new Config(homePath.resolve(Paths.get(configName)));
            entryConfig.setGamePath(homePath);
        } else {
            entryConfig = new Config(homePath.resolve(Paths.get(appName, configName)));
        }
    }

    @Override
    public URL getLogo() {
        return getClass().getResource(headerPath + "/headerra.png");
    }

    @Override
    public Game getGame() {
        return Game.RR_RIDES_AGAIN;
    }

    @Override
    public String[] getFiles() {
        return new String[]{"redneck.grp"};
    }

    @Override
    public String[] getIcons() {
        return new String[]{
                iconPath + "/RRRA/rr16.png",
                iconPath + "/RRRA/rr32.png",
                iconPath + "/RRRA/rr128.png"};
    }

    @Override
    public BuildGame getApplication(List<String> args, String version) throws IOException {
        return new Main(args, entryConfig, appName, version, true);
    }

    @Override
    public GameConfig getConfig() {
        return entryConfig;
    }

    @Override
    public List<String> setGamePath(Path path) {
        entryConfig.setGamePath(path);
        missingFilesCache = checkAndGetMissingFiles(path);
        return missingFilesCache;
    }

    @Override
    public boolean canStart() {
        return missingFilesCache.isEmpty();
    }

    @Override
    public List<String> checkAndGetMissingFiles(Path directoryPath) {
        List<String> missingFiles = GameEntry.super.checkAndGetMissingFiles(directoryPath);
        if (missingFiles.isEmpty()) {
            Path path = directoryPath.resolve("redneck.grp");
            try {
                if (Files.size(path) == 141174222) {
                    missingFiles.add("redneck.grp (wrong)");
                }
            } catch (IOException ignored) {
            }
        }
        return missingFiles;
    }

    @Override
    public void registerStorePath(GameStoreService gameStoreService) {
        gameStoreService.registerGame(Store.GOG, getGame(),
                "[GOG]:\\" + "GOGCREDNECKRIDESAGAIN");

        gameStoreService.registerGame(Store.STEAM, getGame(),
                "[Steam]:\\" + "Redneck Rampage Rides Again" + File.separator + "AGAIN");

    }
}
