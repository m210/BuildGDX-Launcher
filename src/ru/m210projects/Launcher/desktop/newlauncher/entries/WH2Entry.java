package ru.m210projects.Launcher.desktop.newlauncher.entries;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Launcher.desktop.newlauncher.stores.GameStoreService;
import ru.m210projects.Launcher.desktop.newlauncher.stores.Store;
import ru.m210projects.Witchaven.Config;
import ru.m210projects.Witchaven.Main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static ru.m210projects.Launcher.desktop.Main.headerPath;
import static ru.m210projects.Launcher.desktop.Main.iconPath;

public class WH2Entry implements GameEntry {

    private final String appName = "Witchaven2GDX";
    private final GameConfig entryConfig;
    private List<String> missingFilesCache;

    public WH2Entry(Path homePath) {
        missingFilesCache = checkAndGetMissingFiles(homePath);
        boolean portable = missingFilesCache.isEmpty();
        String configName = (appName + ".ini").toLowerCase(Locale.ROOT);
        if (portable) {
            entryConfig = new Config(homePath.resolve(Paths.get(configName)));
            entryConfig.setGamePath(homePath);
        } else {
            entryConfig = new Config(homePath.resolve(Paths.get(appName, configName)));
        }
    }

    @Override
    public URL getLogo() {
        return getClass().getResource(headerPath + "/headerwh2.png");
    }

    @Override
    public Game getGame() {
        return Game.WITCHAVEN_2;
    }

    @Override
    public String[] getFiles() {
        return new String[]{
                "f_songs",
                "joesnd",
                "tiles000.art",
                "tables.dat",
                "palette.dat",
                "lookup.dat",
                "level1.map",
        };
    }

    @Override
    public String[] getIcons() {
        return new String[]{
                iconPath + "/WH2/whii16.png",
                iconPath + "/WH2/whii32.png",
                iconPath + "/WH2/whii128.png"
        };
    }

    @Override
    public BuildGame getApplication(List<String> args, String version) throws IOException {
        return new Main(args, entryConfig, appName, version, true, true);
    }

    @Override
    public GameConfig getConfig() {
        return entryConfig;
    }

    @Override
    public List<String> setGamePath(Path path) {
        entryConfig.setGamePath(path);
        missingFilesCache = checkAndGetMissingFiles(path);
        return missingFilesCache;
    }

    @Override
    public boolean canStart() {
        return missingFilesCache.isEmpty();
    }

    @Override
    public void registerStorePath(GameStoreService gameStoreService) {
        gameStoreService.registerGame(Store.GOG, getGame(),
                "[GOG]:\\" + "Games\\1073977251");

        gameStoreService.registerGame(Store.STEAM, getGame(),
                "[Steam]:\\" + "Witchaven II Blood Vengeance" + File.separator + "Enhanced" + File.separator + "GAME" + File.separator + "WHAVEN2");

    }
}
