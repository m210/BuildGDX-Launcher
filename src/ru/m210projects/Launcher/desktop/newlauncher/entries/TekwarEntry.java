package ru.m210projects.Launcher.desktop.newlauncher.entries;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Tekwar.Config;
import ru.m210projects.Tekwar.Main;

import java.io.IOException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static ru.m210projects.Launcher.desktop.Main.headerPath;
import static ru.m210projects.Launcher.desktop.Main.iconPath;

public class TekwarEntry implements GameEntry {

    private final String appName = "TekWarGDX";
    private final GameConfig entryConfig;
    private List<String> missingFilesCache;
    private boolean isDemo = false;

    public TekwarEntry(Path homePath) {
        missingFilesCache = checkAndGetMissingFiles(homePath);
        boolean portable = missingFilesCache.isEmpty();
        String configName = (appName + ".ini").toLowerCase(Locale.ROOT);
        if (portable) {
            entryConfig = new Config(homePath.resolve(Paths.get(configName)));
            entryConfig.setGamePath(homePath);
        } else {
            entryConfig = new Config(homePath.resolve(Paths.get(appName, configName)));
        }
    }

    @Override
    public URL getLogo() {
        return getClass().getResource(headerPath + "/headertw.png");
    }

    @Override
    public Game getGame() {
        return Game.TEKWAR;
    }

    @Override
    public String[] getFiles() {
        return new String[]{
                "smkmm.smk",
                "tiles000.art",
                "tables.dat",
                "palette.dat",
                "songs",
                "sounds"
        };
    }

    @Override
    public String[] getIcons() {
        return new String[]{
                iconPath + "/Tekwar/tekwar16.png",
                iconPath + "/Tekwar/tekwar32.png",
                iconPath + "/Tekwar/tekwar128.png"
        };
    }

    @Override
    public List<String> checkAndGetMissingFiles(Path directoryPath) {
        isDemo = false;
        List<String> missingFiles = GameEntry.super.checkAndGetMissingFiles(directoryPath);
        if (!missingFiles.isEmpty()) {
            System.out.println(appName + ": trying to find demo version");
            List<String> missingDemoFiles = checkAndGetMissingDemoFiles(directoryPath);
            if (missingDemoFiles.isEmpty()) {
                isDemo = true;
                return missingDemoFiles;
            }
            missingFiles.addAll(missingDemoFiles);
        }
        return missingFiles;
    }

    @Override
    public BuildGame getApplication(List<String> args, String version) throws IOException {
        return new Main(args, entryConfig, appName, version, true, isDemo);
    }

    @Override
    public GameConfig getConfig() {
        return entryConfig;
    }

    @Override
    public List<String> setGamePath(Path path) {
        entryConfig.setGamePath(path);
        missingFilesCache = checkAndGetMissingFiles(path);
        return missingFilesCache;
    }

    @Override
    public boolean canStart() {
        return missingFilesCache.isEmpty();
    }

    /**
     * Tries to find demo game in the directory path
     */
    private List<String> checkAndGetMissingDemoFiles(Path directoryPath) {
        Set<String> missingFiles = new LinkedHashSet<>(Arrays.asList(
                "city.map",
                "tiles000.art",
                "tables.dat",
                "palette.dat",
                "songs",
                "sounds"
        ));
        if (Files.exists(directoryPath)) {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(directoryPath)) {
                stream.forEach(path -> {
                    String fileName = path.getFileName().toString().toLowerCase(Locale.ROOT);
                    try {
                        if (!fileName.equalsIgnoreCase("sounds") || Files.size(path) == 2138112) {
                            missingFiles.remove(fileName);
                        }
                    } catch (IOException ignored) {
                    }
                });
            } catch (IOException e) {
                Console.out.println(String.format("Failed to check path %s: %s", directoryPath, e), OsdColor.RED);
            }
        }
        return new ArrayList<>(missingFiles);
    }
}
