package ru.m210projects.Launcher.desktop.newlauncher.ui;

import org.lwjgl.system.Platform;
import ru.m210projects.Launcher.desktop.newlauncher.Controller;
import ru.m210projects.Launcher.desktop.newlauncher.entries.Game;
import ru.m210projects.Launcher.desktop.newlauncher.entries.GameEntry;
import ru.m210projects.Launcher.desktop.newlauncher.stores.GameLabel;
import ru.m210projects.Launcher.desktop.newlauncher.stores.GameStoreService;
import ru.m210projects.Launcher.desktop.newlauncher.stores.Store;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static ru.m210projects.Build.filehandle.fs.FileEntry.DUMMY_PATH;

public class GamePanel extends JPanel {
    private final GameStoreService gameStoreService;
    private JComboBox<ComboItem> gameFoldersList;
    private JButton choosePathButton;
    private JButton openPathButton;
    private JButton launchButton;
    private JButton settingsButton;
    private JLabel messageField;
    private JPanel gameForm;
    private Game game;

    public GamePanel() {
        add(gameForm);
        this.gameStoreService = new GameStoreService(Platform.get());
        this.gameFoldersList.addPopupMenuListener(new BoundsPopupMenuListener(true, false));
        this.gameFoldersList.setPrototypeDisplayValue(new ComboItem(null, ""));
    }

    public void registerStorePaths(Set<GameEntry> entrySet) {
        for (GameEntry entry : entrySet) {
            entry.registerStorePath(gameStoreService);
        }
    }

    public void initListeners(Controller controller) {
        choosePathButton.addActionListener(e -> controller.onChoosePath());
        openPathButton.addActionListener(e -> controller.onOpenGameFolder());
        launchButton.addActionListener(e -> controller.onStartGame());
        settingsButton.addActionListener(e -> controller.onOpenSettings());

        gameFoldersList.addItemListener(e -> {
            // onSetGamePath will remove items from gameFoldersList that also will triggers this listener
            if (gameFoldersList.isEnabled() && e.getStateChange() == ItemEvent.SELECTED && e.getItem() instanceof ComboItem) {
                // so be sure, that this listener was trig by user (by checking isPopupVisible)
                controller.onSetGamePath(((ComboItem) e.getItem()).getValue());
            }
        });
    }

    public void startGame() {
        launchButton.setText("Loading...");
        launchButton.setEnabled(false);

        gameFoldersList.setEnabled(false);
    }

    public void setGamePath(Path path) {
        launchButton.setText("Play " + game.getShortName());
        launchButton.setEnabled(true);
        messageField.setText("");

        handlePath(path);
    }

    public void showMissingFiles(List<String> missingFiles) {
        launchButton.setEnabled(false);
        launchButton.setText(game.getShortName() + " resources not found!");
        StringBuilder missingFilesMessage = new StringBuilder("<html>");
        for (String name : missingFiles) {
            missingFilesMessage.append(name.toUpperCase()).append(" is missing<br>");
        }
        missingFilesMessage.append("</html>");
        messageField.setText(missingFilesMessage.toString());
    }

    public void chooseGame(Game game) {
        this.game = game;
    }

    private void handlePath(Path path) {
        ComboItem configPath = new ComboItem(path, !path.equals(DUMMY_PATH) ? path.toString() : "");

        final boolean isEnabled = choosePathButton.isEnabled();
        gameFoldersList.setEnabled(false);
        gameFoldersList.removeAllItems();
        gameFoldersList.addItem(configPath);

        SwingUtilities.invokeLater(() -> {
            List<GameLabel> labels = gameStoreService.getGameLabels(game);
            for (GameLabel l : labels) {
                Path gamePath = Paths.get(l.getPath());
                switch (game) {
                    case DUKE_NUKEM_3D:
                        if (l.getStore() == Store.ANTHOLOGY && !gamePath.endsWith("Duke Nukem 3D")) {
                            gamePath = gamePath.resolve("Duke Nukem 3D");
                        }
                        break;
                    case WITCHAVEN:
                        if (l.getStore() == Store.GOG) {
                            gamePath = gamePath.resolve("Enhanced\\GAME\\WHAVEN\\");
                        }
                        break;
                    case WITCHAVEN_2:
                        if (l.getStore() == Store.GOG) {
                            gamePath = gamePath.resolve("Enhanced\\GAME\\WHAVEN2\\");
                        }
                        break;
                }

                ComboItem comboItem = new ComboItem(gamePath, l.getLabel() + " " + gamePath);
                if (Objects.equals(gamePath, path)) {
                    gameFoldersList.removeItem(configPath);
                    gameFoldersList.insertItemAt(comboItem, 0);
                } else {
                    gameFoldersList.addItem(comboItem);
                }
            }
            gameFoldersList.setSelectedIndex(0);
            gameFoldersList.setEnabled(isEnabled);
        });
    }

}
