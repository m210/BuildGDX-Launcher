package ru.m210projects.Launcher.desktop.newlauncher.ui;

import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.lwjgl3.audio.midi.device.SoundBankDevice;
import ru.m210projects.Build.Architecture.common.ResolutionUtils;
import ru.m210projects.Build.Architecture.common.audio.AudioDriver;
import ru.m210projects.Build.Architecture.common.audio.MidiDevice;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Launcher.desktop.newlauncher.Controller;
import ru.m210projects.Launcher.desktop.newlauncher.Utils;
import ru.m210projects.Launcher.desktop.newlauncher.entries.Game;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.io.File;
import java.util.*;
import java.util.List;

public class SettingsPanel extends JPanel {

    private JPanel settingsForm;
    private JComboBox<Graphics.DisplayMode> resolutionComboBox;
    private JComboBox<String> renderComboBox;
    private JCheckBox borderlessCheckBox;
    private JCheckBox fullscreenCheckBox;
    private JComboBox<AudioDriver> soundComboBox;
    private JComboBox<MidiDevice> midiComboBox;
    private JButton deleteSoundFontButton;
    private JButton chooseSoundFontButton;
    private JCheckBox enableAutoloadFolderCheckBox;
    private JButton launchButton;

    public SettingsPanel() {
        add(settingsForm);
        SwingUtilities.invokeLater(() -> {
            for (Renderer.RenderType renderType : Renderer.RenderType.values()) {
                renderComboBox.addItem(renderType.getName());
            }
            soundComboBox.setRenderer(new SoundDriverRenderer());
            midiComboBox.setRenderer(new MidiDeviceCellRenderer());
        });
        launchButton.setVisible(false);
    }

    public void oneGameMode(Game game) {
        launchButton.setVisible(true);
        launchButton.setText("Play " + game.getShortName());
    }

    public void initListeners(Controller controller) {
        renderComboBox.addItemListener(e -> {
            if (renderComboBox.isEnabled() && e.getStateChange() == ItemEvent.SELECTED && e.getItem() instanceof String) {
                controller.onChooseRender(Renderer.RenderType.parseType((String) e.getItem()));
            }
        });

        resolutionComboBox.addItemListener(e -> {
            if (resolutionComboBox.isEnabled() && e.getStateChange() == ItemEvent.SELECTED && e.getItem() instanceof Graphics.DisplayMode) {
                Graphics.DisplayMode displayMode = (Graphics.DisplayMode) e.getItem();
                if (displayMode != null) {
                    controller.onChooseResolution(displayMode);
                }

                fullscreenCheckBox.setEnabled(true);
                if (displayMode instanceof ResolutionUtils.UserDisplayMode) {
                    fullscreenCheckBox.setEnabled(((ResolutionUtils.UserDisplayMode) displayMode).isFullScreenSupport());
                }
            }
        });

        midiComboBox.addItemListener(e -> {
            if (midiComboBox.isEnabled() && e.getStateChange() == ItemEvent.SELECTED && e.getItem() instanceof MidiDevice) {
                controller.onChooseMidiDevice((MidiDevice) e.getItem());
            }
        });

        soundComboBox.addItemListener(e -> {
            if (soundComboBox.isEnabled() && e.getStateChange() == ItemEvent.SELECTED && e.getItem() instanceof AudioDriver) {
                controller.onChooseSound((AudioDriver) e.getItem());
            }
        });

        fullscreenCheckBox.addItemListener(e -> controller.onFullScreenChanged(e.getStateChange() == ItemEvent.SELECTED));
        borderlessCheckBox.addItemListener(e -> controller.onBorderlessChanged(e.getStateChange() == ItemEvent.SELECTED));
        enableAutoloadFolderCheckBox.addItemListener(e -> controller.onEnableAutoloadChanged(e.getStateChange() == ItemEvent.SELECTED));
        chooseSoundFontButton.addActionListener(e -> onChooseSoundFontButton(controller));
        deleteSoundFontButton.addActionListener(e -> controller.onDeleteSoundBank((MidiDevice) midiComboBox.getSelectedItem()));
        launchButton.addActionListener(e -> controller.onStartGame());
    }

    public void openSettings(GameConfig config) {
        Map<String, List<Graphics.DisplayMode>> resolutions = config.getResolutions();
        Graphics.DisplayMode currentMode = new ResolutionUtils.UserDisplayMode(config.getScreenWidth(), config.getScreenHeight());

        // Put the unknown display mode if the default resolutionsMap doesn't have it
        // (to have an ability changing it back in the launcher)
        String configResolution = ResolutionUtils.getDisplayModeAsString(currentMode);
        if (!resolutions.containsKey(configResolution)) {
            resolutions.put(configResolution, Collections.singletonList(currentMode));
        } else {
            currentMode = resolutions.get(configResolution).stream().max(Comparator.comparingInt(a -> a.refreshRate)).orElse(currentMode);
        }

        final String currentModeName = ResolutionUtils.getDisplayModeAsString(currentMode);
        // Disable resolution comboBox listener before remove all items
        resolutionComboBox.setEnabled(false);
        resolutionComboBox.removeAllItems();
        // Add current resolution as unknown (we will check it later). This help sets dimensions for comboBox,
        // while resolution modes loading (to avoid jiggers after updating)
        resolutionComboBox.addItem(currentMode);
        // Unknown resolutions can't switch to fullscreen
        fullscreenCheckBox.setEnabled(false);

        Graphics.DisplayMode finalCurrentMode = currentMode;
        SwingUtilities.invokeLater(() -> {
            for (String resolution : resolutions.keySet()) {
                Graphics.DisplayMode displayMode = resolutions.get(resolution).stream().max(Comparator.comparingInt(a -> a.refreshRate)).orElse(null);
                // try to remove current mode at first (if current selected mode)
                // we have to remove the current mode to make reordering
                resolutionComboBox.removeItem(displayMode);

                // Unknown display mode we have to place at first position
                if ((displayMode instanceof ResolutionUtils.UserDisplayMode) && !((ResolutionUtils.UserDisplayMode) displayMode).isFullScreenSupport()) {
                    resolutionComboBox.insertItemAt(displayMode, 0);
                    continue;
                }

                if (resolution.equalsIgnoreCase(currentModeName)) {
                    // In this case we can enable fullscreen checkbox
                    fullscreenCheckBox.setEnabled(true);
                }
                resolutionComboBox.addItem(displayMode);
            }
            // set current resolution in comboBox
            resolutionComboBox.setSelectedItem(finalCurrentMode);
            // and enable listener
            resolutionComboBox.setEnabled(true);
        });
        renderComboBox.setSelectedItem(config.getRenderType().getName());

        fullscreenCheckBox.setSelected(config.isFullscreen());
        borderlessCheckBox.setSelected(config.isBorderless());
        enableAutoloadFolderCheckBox.setSelected(config.isAutoloadFolder());

        // this button will enable after update if selected device is soundbank
        deleteSoundFontButton.setEnabled(false);
        updateMidiDevices(config.getMidiDevices(), config.getMidiDevice().getName());
        updateAudioDevices(Arrays.asList(AudioDriver.values()), config.getAudioDriver());
    }

    public void updateMidiDevices(List<MidiDevice> deviceList, String selectedDevice) {
        midiComboBox.setEnabled(false);
        midiComboBox.removeAllItems();
        for (MidiDevice device : deviceList) {
            midiComboBox.addItem(device);
            if (device.getName().equalsIgnoreCase(selectedDevice)) {
                checkSelectedSoundBank(device);
                midiComboBox.setSelectedItem(device);
            }
        }
        midiComboBox.setEnabled(true);
    }

    public void updateAudioDevices(List<AudioDriver> audioList, AudioDriver selectedDevice) {
        soundComboBox.setEnabled(false);
        soundComboBox.removeAllItems();
        for (AudioDriver device : audioList) {
            soundComboBox.addItem(device);
            if (device.equals(selectedDevice)) {
                soundComboBox.setSelectedItem(device);
            }
        }
        soundComboBox.setEnabled(true);
    }

    public void setMidiDevice(MidiDevice device) {
        checkSelectedSoundBank(device);
        midiComboBox.setSelectedItem(device);
    }

    private void onChooseSoundFontButton(Controller controller) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setCurrentDirectory(Utils.getDirPath().toFile());
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("SoundFont bank (*.sf2)", "sf2"));
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Downloadable Sounds bank (*.dls)", "dls"));

        int returnVal = fileChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (file != null) {
                controller.onAddSoundBank(file);
            }
        }
    }

    private void checkSelectedSoundBank(MidiDevice device) {
        deleteSoundFontButton.setEnabled(device instanceof SoundBankDevice);
    }

    private static class MidiDeviceCellRenderer extends JLabel implements ListCellRenderer<MidiDevice> {
        @Override
        public Component getListCellRendererComponent(JList<? extends MidiDevice> list, MidiDevice value, int index, boolean isSelected, boolean cellHasFocus) {
            setText(value.getName());
            return this;
        }
    }

    private static class SoundDriverRenderer extends JLabel implements ListCellRenderer<AudioDriver> {
        @Override
        public Component getListCellRendererComponent(JList<? extends AudioDriver> list, AudioDriver value, int index, boolean isSelected, boolean cellHasFocus) {
            setText(value.getName());
            return this;
        }
    }
}
