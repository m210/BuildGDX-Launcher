//This file is part of BuildGDX.
//Copyright (C) 2017-2023  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//BuildGDX is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//BuildGDX is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with BuildGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Launcher.desktop.newlauncher.ui;

import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class DirectoryBrowser extends JDialog implements TreeExpansionListener, TreeSelectionListener, TreeCellRenderer {
    private final DefaultTreeCellRenderer renderer;
    private final ImageIcon gameFolderIcon;
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTree tree;

    private DirectoryBrowser(JFrame f, Path path, String name, Image icon) {
        this.setContentPane(contentPane);
        this.setModal(true);
        this.getRootPane().setDefaultButton(buttonOK);
        this.setTitle("Select " + name + " folder");
        this.setIconImage(icon);

        this.renderer = (DefaultTreeCellRenderer) tree.getCellRenderer();
        this.gameFolderIcon = new ImageIcon(icon);
        this.tree.setCellRenderer(this);

        DefaultMutableTreeNode root = new DefaultMutableTreeNode(name);
        FileSystems.getDefault().getRootDirectories().forEach(p -> root.add(new DirectoryNode(p)));
        final DefaultTreeModel model = new DefaultTreeModel(root);
        tree.setModel(model);

        DirectoryNode directoryNode = findNodeByPath(model, path);
        if (directoryNode != null) {
            TreePath treePath = new TreePath(directoryNode.getPath());
            tree.setSelectionPath(treePath);
            tree.scrollPathToVisible(treePath);
        }

        tree.addTreeExpansionListener(this);
        tree.addTreeSelectionListener(this);

        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        this.pack();
        this.setLocationRelativeTo(f);
        this.setMinimumSize(new Dimension(300, 300));
        this.setVisible(true);
    }

    public static Path showDirectoryChooser(JFrame f, Path path, String name, URL icon) {
        DirectoryBrowser dc = new DirectoryBrowser(f, path, name, Toolkit.getDefaultToolkit().getImage(icon));
        return dc.getSelectedPath(path);
    }

    public Path getSelectedPath(Path defaultPath) {
        Object o = tree.getLastSelectedPathComponent();
        if (o instanceof DirectoryNode) {
            return ((DirectoryNode) o).getDirectoryPath();
        }
        return defaultPath;
    }

    private DirectoryNode findNodeByPath(DefaultTreeModel model, Path path) {
        DirectoryNode directoryNode = null;
        TreeNode nextNode = (TreeNode) model.getRoot();
        while (nextNode != null) {
            TreeNode currentNode = nextNode;
            nextNode = null;

            final int childCount = model.getChildCount(currentNode);
            for (int i = 0; i < childCount; i++) {
                DirectoryNode node = (DirectoryNode) model.getChild(currentNode, i);
                if (path.startsWith(node.directory)) {
                    scanChildren(model, node);
                    directoryNode = node;
                    nextNode = node;
                    break;
                }
            }
        }
        return directoryNode;
    }

    private void scanChildren(DefaultTreeModel model, DirectoryNode n) {
        if (n.scanned) {
            return;
        }

        try (Stream<Path> stream = Files.list(n.directory).filter(Files::isDirectory).filter(Files::isReadable)) {
            stream.forEach(path -> model.insertNodeInto(new DirectoryNode(path), n, n.getChildCount()));
        } catch (IOException e) {
            Console.out.println(String.format("Failed accessing to directory %s: %s", n.directory, e), OsdColor.RED);
        }
        n.scanned = true;
    }

    private void treeScanNode(TreePath tp) {
        Object o = tp.getLastPathComponent();
        if (o instanceof DirectoryNode) {
            scanChildren((DefaultTreeModel) tree.getModel(), (DirectoryNode) o);
        }
    }

    @Override
    public void treeExpanded(TreeExpansionEvent event) {
        treeScanNode(event.getPath());
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        treeScanNode(e.getPath());
    }

    @Override
    public void treeCollapsed(TreeExpansionEvent event) {
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        Component c = renderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        if (value instanceof DirectoryNode && ((DirectoryNode) value).isGameFolder()) {
            renderer.setIcon(gameFolderIcon);
        }
        return c;
    }

    private void onOK() {
        dispose();
    }

    private void onCancel() {
        tree.setSelectionPath(null);
        dispose();
    }

    private static class DirectoryNode extends DefaultMutableTreeNode {
        private final Path directory;
        private boolean scanned = false;
        private boolean gameFolder = false;

        public DirectoryNode(Path path) {
            super(path.getFileName() != null ? path.getFileName().toString() : path.toString());
            this.directory = path;
        }

        public boolean isGameFolder() {
            return gameFolder;
        }

        public void setGameFolder(boolean gameFolder) {
            this.gameFolder = gameFolder;
        }

        public Path getDirectoryPath() {
            return directory;
        }

        @Override
        public boolean isLeaf() {
            return false;
        }
    }
}
