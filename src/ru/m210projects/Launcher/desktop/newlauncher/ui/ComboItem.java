package ru.m210projects.Launcher.desktop.newlauncher.ui;

import java.nio.file.Path;

public class ComboItem {
    private final Path value;
    private final String label;

    public ComboItem(Path value, String label) {
        this.value = value;
        this.label = label;
    }

    public Path getValue() {
        return this.value;
    }

    public String getLabel() {
        return this.label;
    }

    @Override
    public String toString() {
        return label;
    }
}