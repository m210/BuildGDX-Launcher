package ru.m210projects.Launcher.desktop.newlauncher.ui;

import ru.m210projects.Launcher.desktop.Main;
import ru.m210projects.Launcher.desktop.newlauncher.Controller;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.util.Locale;

public class AboutPanel extends JPanel {
    private JButton m210Button;
    private JButton discordButton;
    private JButton duke4Button;
    private JPanel aboutForm;
    private JLabel buildGdxLabel;

    public AboutPanel() {
        add(aboutForm);
        this.buildGdxLabel.setText("BuildGDX " + Main.appversion);
    }

    public void initListeners(Controller controller) {
        m210Button.addActionListener(e -> controller.onOpenSite());
        discordButton.addActionListener(e -> controller.onOpenDiscord());
        duke4Button.addActionListener(e -> controller.onOpenForum());
    }

}
