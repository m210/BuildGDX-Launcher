package ru.m210projects.Launcher.desktop.newlauncher.ui;

import ru.m210projects.Launcher.desktop.Main;

import javax.swing.*;
import java.awt.event.ActionListener;

public class DownloadDialog extends JDialog {
    private JProgressBar progressBar1;
    private JPanel contentPane;
    private JButton btnCancel;

    public DownloadDialog() {
        super(null, "Downloading...", ModalityType.MODELESS);
        setContentPane(contentPane);
        setResizable(false);
        setIconImages(Main.getIconImages());
        pack();
        setLocationRelativeTo(null);
    }

    public void setProgress(int progress) {
        progressBar1.setValue(progress);
    }

    public void setCancelListener(ActionListener listener) {
        btnCancel.addActionListener(listener);
    }
}
