package ru.m210projects.Launcher.desktop.newlauncher.ui;

public class HelpException extends RuntimeException {
    private String description;
    public HelpException(String description) {
        super(description);
    }
}
