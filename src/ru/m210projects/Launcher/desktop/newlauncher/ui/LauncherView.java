package ru.m210projects.Launcher.desktop.newlauncher.ui;

import org.lwjgl.system.Platform;
import ru.m210projects.Build.Architecture.DialogUtil;
import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.Architecture.common.audio.MidiDevice;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Launcher.desktop.Main;
import ru.m210projects.Launcher.desktop.newlauncher.Controller;
import ru.m210projects.Launcher.desktop.newlauncher.View;
import ru.m210projects.Launcher.desktop.newlauncher.entries.Game;
import ru.m210projects.Launcher.desktop.newlauncher.entries.GameEntry;
import ru.m210projects.Launcher.desktop.newlauncher.update.CheckUpdateResponse;
import ru.m210projects.Launcher.desktop.newlauncher.update.ModelEventListener;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import static ru.m210projects.Launcher.desktop.Main.iconPath;

public class LauncherView extends JFrame implements View, ModelEventListener {
    private Controller controller;
    private GamePanel gamePanel;
    private AboutPanel aboutPanel;
    private SettingsPanel settingsPanel;
    private JPanel root;
    private JButton bloodButton;
    private JButton duke3DButton;
    private JPanel logoPanel;
    private JPanel contextPanel;
    private JButton aboutButton;
    private JButton namButton;
    private JButton shadowWarriorButton;
    private JButton redneckRampageButton;
    private JButton rrRidesAgainButton;
    private JButton powerslaveButton;
    private JButton tekWarButton;
    private JButton witchavenButton;
    private JButton witchaven2Button;
    private JButton sevenPaladinsButton;
    private JButton backButton;
    private JPanel buttonPanel;

    public LauncherView(Set<GameEntry> entrySet) {
        setContentPane(root);
        setTitle("BuildGDX " + Main.appversion);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        URL iconUrl = getClass().getResource("/" + iconPath + "/build32.png");
        if (iconUrl != null) {
            setIconImage(new ImageIcon(iconUrl).getImage());
        }

        for (GameEntry entry : entrySet) {
            JLabel gameLabel = new JLabel();
            URL location = entry.getLogo();
            if (location == null) {
                continue;
            }
            gameLabel.setIcon(new ImageIcon(location));
            logoPanel.add(gameLabel, "logo" + entry.getGame().getShortName());
        }

        gamePanel.registerStorePaths(entrySet);
    }

    public void oneGameMode(Game game) {
        buttonPanel.setVisible(false);
        settingsPanel.oneGameMode(game);
    }

    public void start() {
        bloodButton.addActionListener(e -> controller.onChooseGame(Game.BLOOD));
        duke3DButton.addActionListener(e -> controller.onChooseGame(Game.DUKE_NUKEM_3D));
        namButton.addActionListener(e -> controller.onChooseGame(Game.NAM));
        shadowWarriorButton.addActionListener(e -> controller.onChooseGame(Game.SHADOW_WARRIOR));
        redneckRampageButton.addActionListener(e -> controller.onChooseGame(Game.REDNECK_RAMPAGE));
        rrRidesAgainButton.addActionListener(e -> controller.onChooseGame(Game.RR_RIDES_AGAIN));
        powerslaveButton.addActionListener(e -> controller.onChooseGame(Game.POWERSLAVE));
        tekWarButton.addActionListener(e -> controller.onChooseGame(Game.TEKWAR));
        witchavenButton.addActionListener(e -> controller.onChooseGame(Game.WITCHAVEN));
        witchaven2Button.addActionListener(e -> controller.onChooseGame(Game.WITCHAVEN_2));
        sevenPaladinsButton.addActionListener(e -> controller.onChooseGame(Game.LEGEND_OF_THE_SEVEN_PALADINS));
        aboutButton.addActionListener(e -> controller.onOpenAbout());
        backButton.addActionListener(e -> controller.onBack());
        aboutPanel.initListeners(controller);
        gamePanel.initListeners(controller);
        settingsPanel.initListeners(controller);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public Path showDirectoryChooser(Path path, GameEntry entry) {
        return DirectoryBrowser.showDirectoryChooser(this, path, entry.getGame().getName(), ClassLoader.getSystemResource(entry.getIcons()[0]));
    }

    @Override
    public void startGame(Runnable launchCallback) {
        gamePanel.startGame();
        SwingUtilities.invokeLater(() -> {
            dispose();
            launchCallback.run();
        });
    }

    @Override
    public void chooseGame(Game game) {
        gamePanel.chooseGame(game);

        ((CardLayout) logoPanel.getLayout()).show(logoPanel, "logo" + game.getShortName());
        ((CardLayout) contextPanel.getLayout()).show(contextPanel, "gamePanel");

        aboutButton.setEnabled(true);
    }

    @Override
    public void openSettings(GameConfig config) {
        settingsPanel.openSettings(config);
        ((CardLayout) contextPanel.getLayout()).show(contextPanel, "settingsPanel");

        aboutButton.setEnabled(true);
    }

    @Override
    public void setBackButtonEnable(boolean enable) {
        backButton.setEnabled(enable);
    }

    @Override
    public void setGamePath(Path path) {
        gamePanel.setGamePath(path);
    }

    @Override
    public void openAbout() {
        ((CardLayout) logoPanel.getLayout()).show(logoPanel, "logoBuild");
        ((CardLayout) contextPanel.getLayout()).show(contextPanel, "aboutPanel");

        aboutButton.setEnabled(false);
    }

    @Override
    public void openWebpage(String address) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(new URL(address).toURI());
            } catch (Exception ignore) {
            }
        }
    }

    @Override
    public void openFolder(Path path) {
        try {
            if (Platform.get() == Platform.WINDOWS) {
                Runtime.getRuntime().exec("explorer " + path);
            } else {
                Desktop.getDesktop().open(path.toFile());
            }
        } catch (IOException ignore) {
        }
    }

    @Override
    public void showMissingFiles(List<String> missingFiles) {
        gamePanel.showMissingFiles(missingFiles);
    }

    @Override
    public void updateMidiDevices(List<MidiDevice> deviceList, String selectedDevice) {
        settingsPanel.updateMidiDevices(deviceList, selectedDevice);
    }

    @Override
    public void setMidiDevice(MidiDevice device) {
        settingsPanel.setMidiDevice(device);
    }

    @Override
    public boolean onUpdateResult(CheckUpdateResponse response, String resultText) {
        switch (response) {
            case UPDATE_AVAILABLE:
                setTitle("BuildGDX " + Main.appversion + " (new BuildGDX " + resultText + ")");
                DialogUtil.DialogResult result = Main.showMessage("New version available", "Do you want to download new version of BuildGDX " + resultText + "?", MessageType.Question);
                return result.isApproved();
            case UPDATE_ERROR:
                Main.showMessage("Update error", resultText, MessageType.Error);
                break;
        }
        return false;
    }
}
