package ru.m210projects.Launcher.desktop.newlauncher.ui;

import javax.swing.*;
import java.awt.event.*;

public class HelpDialog extends JFrame {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextPane textPane;

    public HelpDialog(String text) {
        setTitle("BuildGDX help");
        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);
        textPane.setText(text);

        buttonOK.addActionListener(e -> onOK());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onOK();
            }
        });

        contentPane.registerKeyboardAction(e -> onOK(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void onOK() {
        dispose();
    }
}
