package ru.m210projects.Launcher.desktop.newlauncher;

import ru.m210projects.Build.Architecture.DialogUtil;
import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.Architecture.common.audio.MidiDevice;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Launcher.desktop.newlauncher.entries.GameEntry;
import ru.m210projects.Launcher.desktop.newlauncher.entries.Game;

import java.nio.file.Path;
import java.util.List;

public interface View {

    void setController(Controller controller);

    Path showDirectoryChooser(Path path, GameEntry entry);

    void startGame(Runnable launchCallback);

    void chooseGame(Game game);

    void openWebpage(String address);

    void openFolder(Path path);

    void openSettings(GameConfig config);

    void openAbout();

    void setBackButtonEnable(boolean enable);

    void setGamePath(Path path);

    void showMissingFiles(List<String> missingFiles);

    void updateMidiDevices(List<MidiDevice> deviceList, String selectedDevice);

    void setMidiDevice(MidiDevice device);
}
