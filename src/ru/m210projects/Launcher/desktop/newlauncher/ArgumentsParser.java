package ru.m210projects.Launcher.desktop.newlauncher;

import ru.m210projects.Launcher.desktop.newlauncher.entries.Game;
import ru.m210projects.Launcher.desktop.newlauncher.ui.HelpException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static ru.m210projects.Build.filehandle.fs.FileEntry.DUMMY_PATH;

public class ArgumentsParser {

    private final Path path;
    private final Game game;
    private final boolean showSettings;
    private final boolean forceUpdate;
    private final boolean updateSuccess;
    private final List<String> gameArgs = new ArrayList<>();

    public ArgumentsParser(String[] args) {
        Path path = DUMMY_PATH;
        Game game = Game.NULL;
        boolean showLauncher = false;
        boolean forceUpdate = false;
        boolean updateSuccess = false;
        for (int i = 0; i < args.length; i++) {
            String s = args[i];
            if (s.equalsIgnoreCase("-path") && (i + 1) < args.length) {
                path = Paths.get(args[++i]);
                if (!Files.exists(path)) {
                    throw new RuntimeException(String.format("Chosen path \"%s\" is not exist!", path));
                }
            } else if (s.equalsIgnoreCase("-?") || s.equalsIgnoreCase("/?") || s.equalsIgnoreCase("-help")) {
                StringBuilder sb = new StringBuilder();
                sb.append("<b>-game \"name\"</b> Force to start game without showing the launcher. The parameter should be used with <b>-path</b>").append("<br>");
                for (Game g : Game.values()) {
                    if (g == Game.NULL) {
                        continue;
                    }

                    Set<String> names = new LinkedHashSet<>();
                    names.add(g.name().toUpperCase(Locale.ROOT));
                    names.add(g.getShortName().toUpperCase(Locale.ROOT));
                    names.add(g.getName().toUpperCase(Locale.ROOT));

                    sb.append("&emsp;&emsp;&emsp;&emsp;").append(g.getShortName()).append(" (");
                    StringJoiner nameJoiner = new StringJoiner(", ");
                    for (String n : names) {
                        nameJoiner.add(n);
                    }
                    sb.append(nameJoiner);
                    sb.append(") <br>");
                }
                sb.append("<b>-path \"path\"</b> path to the game, that you want to start (relative paths is supporting)").append("<br>");
                sb.append("<b>-settings</b> force to show launcher in case when you use <b>-game</b> and <b>-path</b> parameters").append("<br>");
                sb.append("<b>-name \"name\"</b> sets player name").append("<br>");
                sb.append("<b>-players \"player numbers\"</b> sets multiplayer number of players").append("<br>");
                sb.append("<b>-ip \"ip_address\"</b> sets ip address for multiplayer game").append("<br>");
                sb.append("<b>-port \"port\"</b> sets port for multiplayer game").append("<br>");
                sb.append("<b>-netmode \"master\" or \"slave\"</b> starts multiplayer game as server or client (should be used with <b>-ip</b> and <b>-port</b> parameters").append("<br>");

                throw new HelpException(sb.toString());
            } else if (s.equalsIgnoreCase("-settings")) {
                showLauncher = true;
            } else if (s.equalsIgnoreCase("-game") && (i + 1) < args.length) {
                String gameName = args[++i];
                game = Game.parseGame(gameName);
                if (game == Game.NULL) {
                    throw new RuntimeException(String.format("Game \"%s\" is not found!", gameName));
                }
            } else if (s.equalsIgnoreCase("-force_update")) {
                forceUpdate = true;
            } else if (s.equalsIgnoreCase("-updated")) {
                updateSuccess = true;
            } else {
                gameArgs.add(s);
            }
        }

        this.path = path;
        this.game = game;
        this.showSettings = showLauncher;
        this.forceUpdate = forceUpdate;
        this.updateSuccess = updateSuccess;
    }

    public boolean isUpdateSuccess() {
        return updateSuccess;
    }

    public boolean isForceUpdate() {
        return forceUpdate;
    }

    public List<String> getOtherArguments() {
        return gameArgs;
    }

    public Game getGame() {
        return game;
    }

    public Path getPath() {
        return path;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean showLauncher() {
        return showSettings;
    }
}
