//This file is part of BuildGDX.
//Copyright (C) 2023  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//BuildGDX is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//BuildGDX is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with BuildGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Launcher.desktop;

import com.badlogic.gdx.backends.awt.AWTDialog;
import ru.m210projects.Build.Architecture.DialogUtil;
import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Launcher.desktop.newlauncher.ArgumentsParser;
import ru.m210projects.Launcher.desktop.newlauncher.Controller;
import ru.m210projects.Launcher.desktop.newlauncher.LauncherModel;
import ru.m210projects.Launcher.desktop.newlauncher.entries.Game;
import ru.m210projects.Launcher.desktop.newlauncher.ui.HelpDialog;
import ru.m210projects.Launcher.desktop.newlauncher.ui.HelpException;
import ru.m210projects.Launcher.desktop.newlauncher.ui.LauncherView;
import ru.m210projects.Launcher.desktop.newlauncher.update.VersionChecker;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Main {

    public static final String headerPath = "/Headers";
    public static final String iconPath = "Games";
    public static String appversion = "v1.18";

    public static void main(final String[] args) throws IOException {
        long start = System.nanoTime();

        LauncherModel model;
        ArgumentsParser argumentsParser;
        try {
            argumentsParser = new ArgumentsParser(args);
            if (argumentsParser.isUpdateSuccess()) {
                showMessage("BuildGDX updater", "BuildGDX is successfully updated to " + Main.appversion, MessageType.Info);
            }
            model = new LauncherModel(argumentsParser);
        } catch (HelpException helpException) {
            new HelpDialog(helpException.getMessage());
            return;
        } catch (Exception e) {
            e.printStackTrace();
            showMessage("Initialization error", e.toString(), MessageType.Error);
            return;
        }

        // If the launcher was launch in a game directory and showing launcher is disabled, start the portable game
        // #GDX 11.01.2025 Added force_update arg to test updating new version
        boolean forceUpdate = argumentsParser.isForceUpdate();
        if (model.getPortableEntry() != null || forceUpdate) {
            VersionChecker checker = new VersionChecker((response, resultText) -> {
                switch (response) {
                    case UPDATE_AVAILABLE:
                        if (showMessage("New version available",
                                "Do you want to download new version of BuildGDX " + resultText + "?",
                                MessageType.Question).isApproved()) {
                            return true;
                        }
                        break;
                    case UPDATE_CANCELED:
                        showMessage("BuildGDX updater", resultText, MessageType.Info);
                        break;
                    case UPDATE_ERROR:
                        showMessage("Update error", resultText, MessageType.Error);
                        break;
                }

                if (!forceUpdate) {
                    model.launchGame();
                }
                return false;
            });

            // Start check. Will block thread while modal dialog shown
            if (forceUpdate) {
                checker.runUpdate();
            } else {
                checker.run();
            }

            System.out.println((System.nanoTime() - start) / 1000000.);
            return;
        }

        // Show launcher normally
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Throwable ignored) {
        }

        Controller controller = new Controller(model);
        LauncherView view = new LauncherView(model.getEntries());
        controller.setView(view);
        // Checks new version in another thread to avoid frontend blocking
        new Thread(new VersionChecker(view)).start();
        if (model.getPortableEntry() != null) {
            // Go here if game can start and should show settings panel
            Game game = model.getPortableEntry().getGame();
            view.oneGameMode(game);
            controller.onChooseGame(game);
            controller.onOpenSettings();
            controller.resetBackStack();
        }
        view.start();

        System.out.println((System.nanoTime() - start) / 1000000.);
    }

    public static DialogUtil.DialogResult showMessage(String title, String message, MessageType messageType) {
        AWTDialog dialog = new AWTDialog(null);
        dialog.setIconImages(getIconImages());
        return dialog.showMessage(title, message, messageType);
    }

    public static List<Image> getIconImages() {
        return Collections.singletonList(new ImageIcon(Objects.requireNonNull(Main.class.getResource("/" + iconPath + "/build32.png"))).getImage());
    }
}
